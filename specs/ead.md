# EMPAIA App Description (EAD)

The source code and EAD for all scenarios mentioned in the tutorial sections can be downloaded from the [Sample Apps Repository (Tutorial: Backend)](https://gitlab.com/empaia/integration/sample-apps/-/tree/master/sample_apps/tutorial) and used as template.

The full EAD JSON schema can be obtained from the [Definitions Repository](https://gitlab.com/empaia/integration/definitions/-/blob/main/ead/ead-schema.v3.json).

## References

If you are curious on how references might be used, have a look at [Purpose of References](specs/references.md#).

The table below shows possible references between the different data types (read: "line has reference onto column"):

|                       | wsi | collection | primitive data types | annotation data types | class | pixelmap data types |
| --------------------- | --- | ---------- | -------------------- | --------------------- | ----- | ------------------- |
| wsi                   | -   | -          | -                    | -                     | -     | -                   |
| collection            | ×   | -          | -                    | ×                     | -     | -                   |
| primitive data types  | ×   | ×          | -                    | ×                     | -     | -                   |
| annotation data types | ×!  | -          | -                    | -                     | -     | -                   |
| class                 | -   | -          | -                    | ×!                    | -     | -                   |
| pixelmap data types   | ×!  | -          | -                    | -                     | -     | -                   |

?> The reference parameter is optional but enhances interpretability of your app and its results. Its effect differs depending if it is used within an input or output parameter:

* Inputs use references as constraint.
* If the reference parameter is left out, no reference-constraint will be used to validate input data.
* Outputs use reference for semantic purposes.
* If the reference parameter is left out, you will NOT be able to include a reference when sending your results. Therefore it will not be possible to determine which value refers to which rectangle annotation.

!> For annotation inputs and outputs, references are **NOT OPTIONAL** and **MUST** reference a WSI.

!> For class inputs and outputs, references are **NOT OPTIONAL** and **MUST** reference an annotation.

!> For pixelmap inputs and outputs, references are **NOT OPTIONAL** and **MUST** reference a WSI.

## Namespace

The namespace of an app **MUST** be included in the EAD and **MUST** be unique to unambiguously indentify the app and the specific app version.

```JSON
{
    "namespace": "org.empaia.<vendor_name>.<app_name>.<app_version>"
}
```

Remarks:

* **MUST** begin with `org.empaia`
* `<vendor_name>` can be used to define a short naming schema of your company
  * **MUST NOT** include `.`
* `<app_name>` can be used to define a short naming schema of your app
  * **MUST NOT** include `.`
* `<app_version>` **MUST** specify the version of your app
  * **MUST** start with `v`
  * a valid version **MUST** be specified as: `v<APP_API_VERSION>.<APP_VERSION>`
    * the `APP_API_VERSION` is the version of the App API the app was developed for
      * the version of the App API can be retrieved from the [App API OpenAPI specification](specs/app_api.md#/openapi-specification)
    * the `APP_VERSION` is a consecutive number to indentify different versions of an app
      * **MUST NOT** include `.`

Example:

```JSON
{
    "namespace": "org.empaia.my_vendor.my_app_01.v3.0"
}
```

## IO

All data structures, whether input or output, that are defined in the app modes section must be declared within the `"io"` section of the EAD.

```JSON
"io": {
    "my_wsi": {
        "type": "wsi"
    },
    "my_rectangle": {
        "type": "rectangle",
        "reference": "io.my_wsi"
    },
    "tumor_cell_count": {
        "type": "integer"
    }
}
```

References to other data types **MUST** always begin with `io.`.

Required:

* `"io"` property must be present within the EAD

## App Modes

An app **MUST** declare the app modes it supports in the `"modes"` property.

Currently, these app modes are available:

* `standalone`
* `preprocessing`
* `postprocessing`
* `report`

?> For more information about app modes see [here](/specs/app_modes.md#)

```JSON
"modes": {
    "preprocessing": {
        "inputs": [
            "my_wsi"
        ],
        "outputs": [
            "my_cells",
            "my_cell_classes"
        ]
    },
    "standalone": {
        "inputs": [
            "my_wsi",
            "my_rectangle"
        ],
        "outputs": [
            "my_cells",
            "my_cell_classes",
            "tumor_ratio"
        ]
    },
    "postprocessing": {
        "containerized": false,
        "inputs": [
            "my_wsi",
            "my_rectangle",
            "my_cells",
            "my_cell_classes"
        ],
        "outputs": [
            "tumor_ratio"
        ]
    }
}
```

Required:

* at least one app mode must be declared
* mode `postprocessing` is only valid if `preprocessing` is also declared
* each mode **MUST** contain the properties `"inputs"` and `"outputs"` to specify the respective data
  * data is referenced by its respective key in the `"io"` section of the EAD and **MUST** be defined there

## Data Types

This section includes detailed information about all data types available and how to use them in the EAD.

### WSI Data Type

```JSON
{
    "my_wsi": {
        "type": "wsi",
        "description": "Human readable description",
    }
}
```

Required:

* `type` is `wsi`

### Primitive Data Types

#### Integer

```JSON
{
    "my_int": {
        "type": "integer",
        "description": "Human readable description",
        "reference": "io.some_other_param"
    }
}
```

Required:

* `type` is `integer`

#### Float

```JSON
{
    "my_float": {
        "type": "float",
        "description": "Human readable description",
        "reference": "io.some_other_param"
    }
}
```

Required:

* `type` is `float`

#### Bool

```JSON
{
    "my_bool": {
        "type": "bool",
        "description": "Human readable description",
        "reference": "io.some_other_param"
    }
}
```

Required:

* `type` is `bool`

#### String

```JSON
{
    "my_string": {
        "type": "string",
        "description": "Human readable description",
        "reference": "io.some_other_param"
    }
}
```

Required:

* `type` is `string`

### Annotation Data Types

For all annotation types:

```JSON
{
    "my_point": {
        "type": "Annotation type",
        "description": "Human readable description",
        "reference": "io.some_wsi_param",
        "classes": [
            "org.empaia.my_vendor.my_app.v3.0.classes.arbitrarily.nested.class",
            "org.empaia.global.v1.classes.roi",
        ]
    }
}
```

Required:

* `type` (one of `point`, `line`, `arrow`, `rectangle`, `polygon`, `circle`)
* `reference` pointing to a WSI or items of a WSI collection

Optional:

The `classes` field defines a class constraint for the specified annotation. This stipulates that one (or more) of the given classes is assigned to the annotation. A class constraint may refer to any node in the class hierarchy and thereby match any class underneath. For example, the class `org.empaia.my_vendor.my_app.v3.0.classes.arbitrarily.nested.class` would fulfill the constraint `org.empaia.my_vendor.my_app.v3.0.classes.arbitrarily`.

### Collection Data Type

```JSON
{
    "my_collection": {
        "type": "collection",
        "description": "Human readable description",
        "reference": "io.some_other_param",
        "items": {
            "type": "any_type"
            // remaining properties depending on type
        }
    }
}
```

Required:

* `type` is `collection`
* `items`: Inner object can be any data type, including `collection`

### Class Data Type

```JSON
{
    "namespace": "org.empaia.my_vendor.my_app.v3.0",
    "classes": {
        "tumor": {
            "pos": {
                "name": "Tumor positive",
                "description": "Human readable description"
            },
            "neg": {
                "name": "Tumor negative",
                "description": "Human readable description"
            }
        },
        "non_tumor": {
            "name": "Non tumor",
            "description": "Human readable description"
        }
    },
    "io": {
        "my_class": {
            "type": "class",
            "description": "Human readable description",
            "reference": "io.some_annotation_param"
        }
    }
}
```

Required:

* `type` is `class`
* `reference` to an annotation or items of an annotation collection
* `classes` on top level must define all App specific classes. These can be arbitrarily nested. EMPAIA classes can always be used and must not be defined!

### Pixelmap Data Types

[Pixelmaps](glossary.md#pixelmap) are fully defined in [EMP-0102](https://gitlab.com/empaia/emps/-/blob/main/emp-0102.md).

```JSON
{
    "namespace": "org.empaia.my_vendor.my_app.v3.0",
    "io": {
        "my_pixelmap": {
            "type": "nominal_pixelmap",
            "description": "Human readable description",
            "reference": "io.some_wsi_param",
            "channel_classes": [
                "org.empaia.my_vendor.my_app.v3.0.classes.arbitrarily.nested.class",
            ],
            "element_classes": [
                "org.empaia.my_vendor.my_app.v3.0.classes.arbitrarily.nested.class",
            ]
        }
    }
}
```

Required:

* `type` is one of `continuous_pixelmap`, `discrete_pixelmap`, `nominal_pixelmap`
* `reference` to a `wsi` or items of a `wsi` collection

Optional:

The EAD fields `channel_classes` (all pixelmap data types) and `element_classes` (only `discrete_pixelmap` and `nominal_pixelmap`) are optional constraints, that are used to validate the correctness of job inputs and outputs. These EAD fields are equivalent to the `classes` constraints for annotation data types. If `channel_classes` or `element_classes` are defined in the EAD, any class matching the constraint must be set in the pixelmap object's `channel_class_mapping` or `element_class_mapping` fields respectively. A class constraint may refer to any node in the class hierarchy and thereby match any class underneath. For example, the class `org.empaia.my_vendor.my_app.v3.0.classes.arbitrarily.nested.class` would fulfill the constraint `org.empaia.my_vendor.my_app.v3.0.classes.arbitrarily`.

## Configuration

The `configuration` property is optional. It enables the definition of global and customer parameters (key value pairs). The values of global configuration parameters are the same for every running instance of the app independent of the execution environment. The values of customer configuration parameters are depending on the organization a user is part of.

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "My app",
    "namespace": "org.empaia.my_vendor.my_app.v3.0",
    "description": "Human readable description",
    "configuration": {
        "global": {
            "private_api_username": {
                "type": "string",
                "optional": false
            },
            "private_api_password": {
                "type": "string",
                "optional": false
            },
        },
        "customer": {
            "customer_username": {
                "type": "string",
                "optional": false
            },
            "customer_password": {
                "type": "string",
                "optional": false
            }
        }
    },
    "io": {
        // (...)
    },
    "modes": {
        // (...)
    }
}
```

Required:

* must contain configuration scope (at least one of `global`, `customer`)
* for each configuration parameter:
  * `"type"` is one of `boolean`, `integer`, `float`, or `string`

## Permissions

If the app needs special permissions, the property `"permissions"` is required, e.g. `data_transmission_to_external_service_provider` must be included and set to `true` to send data to an external API. For further requirements refer to the [app requirements](specs/app_requirements.md#) section.

```json
{
    "permissions": {
        "data_transmission_to_external_service_provider": true
    }
}
```

Apps can download a WSI in its proprietary format as a ZIP file. If an app needs this feature the required permission must be included:

```json
{
    "permissions": {
        "wsi_raw_file_access": true
    }
}
```

!> **Important:** Developing an app that relies on downloading a complete WSI is **not recommended**, as this may not be supported in all environments. Whether the download is possible can be checked using the `raw_download` field in the slide info response. An app that requires the entire WSI for processing should therefore use the endpoint to access the tiles or use the tile endpoint as a fallback if the download is not supported.

Optional:

* permission as key value pair
  * for each permission: set to `true` if needed, otherwise omit

## Rendering Hints

The `rendering` section in the EAD contains hints on which color to choose when displaying `annotations` or `nominal_pixelmaps` based on assigned classes. App UIs are responsible for rendering and may choose to ignore these hints or to provide additional color map alternatives.

```json
{
    "rendering": {
        "annotations": [
            {
                "class_value": "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.tissue",
                "color": "#00FF00",
                "color_hover": "#64FF64",
                "color_selection": "#C8FFC8"
            }
        ],
        "nominal_pixelmaps": [
            {
                "class_value": "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.tissue",
                "color": "#00FF00",
                "color_hover": "#64FF64",
                "color_selection": "#C8FFC8"
            },
            {
                "class_value": "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.cell_nuclei",
                "color": "#FFFF00",
                "color_hover": "#FFFF64",
                "color_selection": "#FFFFC8"
            }
        ]
    }
}
```

Required:

* `class_value`: must be defined in EAD class namespace or in global class namespace
* `color`: primary rendering color in RGB hex format

Optional:

* `color_hover`: secondary color for change on hovering
* `color_selection`: secondary color for change on selection (e.g. click)

## Other fields

The `name` and `description` fields in the EAD should contain english text. They are only used for documentation purposes and not exposed to the user (the presentation of the app in the EMPAIA Portal does not use any data from the EAD).

The `name_short` field is displayed in the Workbench Client and limited to a length of 10 characters. Each app is represented with the vendor logo, the vendor name, the app's `name_short` and the app's version number. Therefore `name_short` must not contain the vendor name, while the `name` field should contain the vendor name for documentation purposes.
