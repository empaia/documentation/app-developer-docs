# Pixelmaps Tile Transfer Compression

The following section covers the posting and requesting of pixelmap binary tile data. Pixelmaps are pixel-wise data visualization overlays for WSIs. Because WSIs can have large dimensions at high resolution levels, the binary tile data of pixelmaps that are transmitted over possibly slow network connections might also get very large.

## Transfer Compression

To address this issue, a client can optionally set specific HTTP Headers in tile data requests:

* If a client sends gzip compressed tiles, the client sets the HTTP request header `Content-Encoding: gzip`. The server detects the header and decompresses the received tiles.
* If a client requests gzip compressed tiles, the clients sets the HTTP request header `Accept-Encoding: gzip`. The server detects the header and compresses the tiles sent in the response body. The server also sets the HTTP response header `Content-Encoding: gzip`.

For more information about Pixelmaps Tile Transfer Compression see [EMP-0107](https://gitlab.com/empaia/emps/-/blob/main/emp-0107.md)
