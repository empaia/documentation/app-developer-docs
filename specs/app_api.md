# App API

The App API is a set of endpoints designed for communication between apps and the Medical Data Service. It provides a job specific view of both the input and output data whereas parametrization is done using identifier keys defined in the EAD.

Through the App API an app can retrieve input data to be processed using the `/inputs/{input_key}` endpoint. For example an app may query regions of interest and their corresponding WSI's tile data. Outputs are then collected using the `/outputs/{output_key}` with the corresponding identifier key. These can also be collections of any nested depth. A `/query` endpoint may further filter collections items, e.g. to only retrieve annotations present in a specific viewport. Output collections can be filled in chunks using a special `/items` endpoint where items can be appended to. This also allows for very large collections without much memory overhead. Finally, apps can report their progress via `/progress` and indicate the finalization of a job by invoking a special `/finalize` endpoint. In case of any error the app can produce a human readable error message by using the corresponding `/failure` endpoint. Apps which require further configuration, e.g. for accessing an external service, may fetch a global configuration as well as a customer-specific configuration using the `/configuration` endpoint.

Functionality of the App API:

* Retrieve the app mode
* Retrieve input data (slides, annotations, classes, primitives, collections, etc.)
* Retrieve slide image data (tile or region based)
* Write output data (annotations, classes, primitives, collections, etc.)
* Indicate finalization of the current job
* Indicate failure of the current job
* Update the progress of a job
* Fetch app and customer specific configuration

## OpenAPI Specification

<a href="specs/app_service_v3_redoc" target="_blank">OpenAPI specification in Redoc</a>

The OpenAPI specification covers most of the API functionality. Additional information regarding correct usage and parametrization of these API endpoints is described below.

## Retrieve WSI Data

The retrieval of WSI data is done analogous to the Workbench API. The usage of these endpoints is addressed in [WSI data](specs/wsi_data.md#). In addition, there is an option for apps to download WSIs in their proprietary format as a ZIP file. This can be done via `GET /slides/{wsi_id}/download`.

?> In order to be able to use the download endpoint, the necessary permission must be requested in the EAD (see [Permissions](/specs/ead.md#permissions)).

## Get Job Mode

Apps can retrieve their mode via the endpoint `GET /mode`. Detailed information on the various app modes, that a job can take, can be found at [App Modes](specs/app_modes.md#).

## Retrieve Job Inputs

Job inputs can be requested through the `GET /inputs/{input_key}` endpoint where the `input_key` references the input given in the EAD. The response can either be a WSI, an annotation, a class, a primitive or a collection containing one of the mentioned types or a collection itself.

For collections the API provides a dedicated endpoint to query collection items based on a set of filters (similar to the filtering mechanism used in the [Workbench API](specs/workbench_api.md#)). In brief we can filter by

* `references` filtering all items that have a reference to a certain id (i.e. all annotations referencing a slide id)
* `viewport` filtering all annotaions that are visible within a certain region (including intersections). This is calculated on the bounding boxes of each annotation, which are calculated when the annotation is persisted.
* `npp_viewing` filtering all annotations that are visible within the resolution range given. See [Annotation Queries](specs/workbench_api.md#annotation-queries).

The collection endpoint provides additional query parameters, that can be used to shrink the collection response:

* When `shallow` is `true` items of the most inner collection will be returned as an empty list
* When `skip` and `limit` are set, it is possible to fetch the collection batchwise (i.e. fetch the first hundred items)

## Post Job Outputs

Persisting data is a major task of the App API as it writes back the result data to the Medical Data Service, that is the common data provider for the App API and Workbench API. To do so, the API comes up with a generic `POST /outputs/{output_key}` endpoint, facilitating the creation of job results. Via the endpoint an app can post annotations, classes, primitives or collections. The following section covers a few key concepts that should be noticed to generate valid job output.

### Primitives

When posting a primitive data type make sure following properties are set correctly:

* `reference_id` is required, if (and only if) specified in the EAD
* `reference_type` is required, if (and only if) `reference_id` is set. Has to be one of `"annotation"`, `"collection"` or `"wsi"`
* `value` is depending on type [`bool`, `int`, `float`, `string`] specified by EAD

### Annotations

When posting an annotation make sure following properties are set correctly:

* `reference_id` **MUST** be a WSI `id`.
* `reference_type` **MUST** be `"wsi"`.
* `centroid` is **opional** and **should** contain a single coordinate representing the given annotation to enable clustering in the frontend. If no custom value is set, the geometrical center of the annotation (i.e. centroid) will be calculated as default value. See [Annotation Queries](specs/workbench_api.md#annotation-queries).

?> All annotation types have the properties `npp_created` and `npp_viewing` (abbreviation for *nanometer per pixel*):

* `npp_created` is **required** and **MUST** contain the pixel resolution level of the WSI-tiles your app used to find the annotations, e.g. `499`
* `npp_viewing` is **optional** and **should** contain a pixel resolution level range within it makes any sense for the annotations to be displayed when a pathologist views the WSI and the annotation, e.g. `[499, 3992]`:
  * `499` would be the base layer resolution if the WSI was scanned using 20x magnification
  * `3992` is the calculated resolution of the base layer multiplied by the downsample factor of level 3 (see below)
* The pixel resolution level can be calculated using the endpoint for the WSI, i.e.:

```python
base_layer_npp = get_input("my_wsi")["pixel_size_nm"]
level_3_downsample = get_input("my_wsi")["levels"][3]["downsample_factor"]
npp_level_3 = base_layer_npp * level_3_downsample
```

!> For further details about `npp_created` and `npp_viewing` see [Annotation Resolution](specs/annotation_resolution.md#)

* `[x, y]` values always refer to pixel coordinates on the base level (level 0) of the WSI, where `[0, 0]` is the top left corner of the WSI.

### Collections

When posting a collection make sure following properties are set correctly:

* `reference_id` **MUST** be set, if (and only if) specified in EAD
* `reference_type` **MUST** be set, if (and only if) `reference_id` is set. One of `"annotation"` or `"wsi"`
* `items`:
  * Includes the collection items (i.e. annotations, nested collections, ids, etc.)
  * Further items can be added via endpoint `POST /collections/{collection_id}/items`

### Classes

When posting a class make sure following properties are set correctly:

* `reference_id` **MUST** be an annotation `id`
* `reference_type` **MUST** be `"annotation"`
* `value` is either a class defined in the EAD or a global EMPAIA class

List of available global EMPAIA classes:

* `org.empaia.global.v1.classes.roi`

See [EMPAIA namespaces](https://gitlab.com/empaia/integration/definitions/-/blob/main/namespaces/org.empaia.global.v1.json) for an up-to-date list.

### Pixelmaps

[Pixelmaps](glossary.md#pixelmap) are fully defined in [EMP-0102](https://gitlab.com/empaia/emps/-/blob/main/emp-0102.md).

#### Requesting Pixelmaps Level and Resolution Info

Under certain circumstances it might not be possible to access level and resolution information of a referenced WSI via the `GET /{job_id}/inputs/{wsi_input_key}` endpoint. In these cases see [Pixelmaps Level and Resolution Info](specs/pixelmap_resolution.md#)

#### Pixelmaps Tile Transfer Compression

For optional Pixelmaps Tile Transfer Compression see [Pixelmaps Tile Transfer Compression](specs/pixelmap_tile_compression.md#)
