# WSI Data

This chapter is focusing on the retrieval of WSI specific data. The slide endpoints provided by the App API as well as the Workbench API are based on a common subset of endpoints.
For the sake of simplicity, we will only provide examples on how to obtain the image data from the App API. For examples related to the Workbench API check the [Frontend Tutorial](tutorial_frontend/workbench_api_usage.md#) and the [Frontend Workspace - SampleApp](https://gitlab.com/empaia/integration/frontend-workspace/-/tree/master/apps/sample-app-v3). The response models shown in the examples below are equal for both APIs.

<!-- tabs:start -->
<!-- tab:App API -->
```python
slide_info_endpoint   = f"{APP_API}/v0/{JOB_ID}/inputs/my_wsi"
slide_tile_endpoint   = f"{APP_API}/v0/{JOB_ID}/tiles/{wsi_id}/level/{level}/position/{pos_x}/{pos_y}"
slide_region_endpoint = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{size_x}/{size_y}"
```

<!-- tab:Workbench API -->
```js
slide_info_endpoint   = "${WBS_API}/v3/scopes/{scope_id}/slides/${wsi_id}/info"
slide_tile_endpoint   = "${WBS_API}/v3/scopes/{scope_id}/slides/${wsi_id}/tile/level/${level}/tile/${pos_x}/${pos_y}"
slide_region_endpoint = "${WBS_API}/v3/scopes/{scope_id}/slides/${wsi_id}/region/level/${level}/start/${start_x}/${start_y}/size/{size_x}/{size_y}"
```
<!-- tabs:end -->

We assume the EAD and the app from the first tutorial section, i.e. [01_Simple_App](tutorial_backend/simple_app.md#), hence, the identifier `my_wsi`.

!> Note that the regions endpoint is restricted to a maximum size of 25.000.000 pixels (e.g. for a square region this means 5000 x 5000 pixels). To obtain regions exceeding this size limit, split the region in smaller subregions and use multiple requests.

## Obtaining the WSI metadata

We assume `my_wsi` to be the Aperio WSI hosted by OpenSlide at:

[OpenSlide - Aperio CMU-1.svs](http://openslide.cs.cmu.edu/download/openslide-testdata/Aperio/CMU-1.svs)

Calling the `slide_info_endpoint` in our app is done as shown below:

```python
from pprint import pprint

# (...) for complete code, see tutorial

url = f"{APP_API}/v0/{JOB_ID}/inputs/my_wsi"
r = requests.get(url, headers=HEADERS)
r.raise_for_status()
my_wsi = r.json()
pprint(my_wsi)
```

This will result in following slide info response:

```JSON
{
    "id": "f985bd96-0f96-41bb-a3ce-af9f04d460f8",
    "channels": [
        {
            "id": 0,
            "name": "Red",
            "color": {
                "r": 255,
                "g": 0,
                "b": 0,
                "a": 0
            }
        },
        {
            "id": 1,
            "name": "Green",
            "color": {
                "r": 0,
                "g": 255,
                "b": 0,
                "a": 0
            }
        },
        {
            "id": 2,
            "name": "Blue",
            "color": {
                "r": 0,
                "g": 0,
                "b": 255,
                "a": 0
            }
        }
    ],
    "channel_depth": 8,
    "extent": {
        "x": 46000,
        "y": 32914,
        "z": 1
    },
    "num_levels": 3,
    "pixel_size_nm": {
        "x": 499,
        "y": 499,
        "z": null
    },
    "tile_extent": {
        "x": 256,
        "y": 256,
        "z": 1
    },
    "levels": [
        {
            "extent": {
                "x": 46000,
                "y": 32914,
                "z": 1
            },
            "downsample_factor": 1
        },
        {
            "extent": {
                "x": 11500,
                "y": 8228,
                "z": 1
            },
            "downsample_factor": 4.000121536217793
        },
        {
            "extent": {
                "x": 2875,
                "y": 2057,
                "z": 1
            },
            "downsample_factor": 16.00048614487117
        }
    ],
    "raw_download": true
}
```

* `extent` describes the dimensions of the whole WSI respectively the dimensions of each particular image level (in x-, y- and z-direction)
* `tile_extent` defines the size of image tiles returend by the tile endpoint (this parameter is usually drawn from the file format itself)
* `pixel_size_nm` defines the size of each pixel in nanometers on the base layer of the WSI. For example: A WSI scanned with a magnification of 20x usually has a pixel size of approximately 500nm, while a slide scanned with 40x measures around 250nm.
* `channels` defines a `name` and a `color` value for separate image channels. This is especially used for multispectral and fluorescence images as described below. By default the R-, G- and B-values are used for ordinary RGB-images.
* `channel_depth` defines the number of bits allocated for each channel of image pixel data. See [Example Fluoresence](#example-fluorescence-wsi).
* `raw_download` defines whether it is possible to download the WSI in its proprietary format as a ZIP file.

### Valid Tile and Region Requests

This information can then be used to request tiles and regions. Valid request would be:

```python
# regions endpoint
wsi_id = my_wsi["id"] # see endpoint_1 above
region_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/0/start/0/0/size/1024/1024"
r = requests.get(tile_url, headers=HEADERS)
img = Image.open(BytesIO(r.content))
```

```python
# tiles endpoint
tile_url = f"{APP_API}/v0/{JOB_ID}/tiles/{wsi_id}/level/0/position/0/0"
```

The following is also valid, but half of the resulting image only consists of white pixels as the region exceeds the WSI's x coordinates `0-2875` of level `2`:

```python
region_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/0/start/2300/0/size/1024/1024"
```

### Invalid Tile and Region Requests

The following endpoints are invalid, as level `3` is not provided by the WSI.

```python
# regions endpoint
region_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/3/start/0/0/size/1024/1024"
```

```python
# tiles endpoint
tile_url = f"{APP_API}/v0/{JOB_ID}/tiles/{wsi_id}/level/3/position/0/0"
```

?> OpenSlide and other libraries might support more levels for this particular slide. But these are artificially downsampled and not natively provided by the WSI, which complicates clinical usage.


## Example Fluorescence WSI

At time of writing fluorescence images are only supported throught the OME-TIFF image format. Proprietary formats like `czi` need to be converted to OME-TIFF first (see [article on image.sc](https://forum.image.sc/t/converting-whole-slide-images-to-ome-tiff-a-new-workflow/32110)).

A sample fluorescence WSI in OME-TIFF format can be found here:

[OpenMicroscopy - OME-TIFF Fluorescence](https://downloads.openmicroscopy.org/images/OME-TIFF/2016-06/sub-resolutions/Fluorescence/)

If `my_wsi` is a fluorescence WSI, the JSON response of `slide_info_endpoint` will differ primarily in terms of image channels and image depth:

```JSON
{
    [...],
    "channels": [
        {
            "id": 0,
            "name": "AF555",
            "color": {
                "r": 130,
                "g": 0,
                "b": 0,
                "a": 0
            }
        },
        {
            "id": 1,
            "name": "AF488",
            "color": {
                "r": 255,
                "g": 51,
                "b": 0,
                "a": 0
            }
        },
        {
            "id": 2,
            "name": "DAPI",
            "color": {
                "r": 160,
                "g": 255,
                "b": 0,
                "a": 0
            }
        }
    ],
    "channel_depth": 16,
    [...],
}
```

Especially for fluorescence images, the parameters `channels` and `channel_depth` are essential. The `channel_depth` defines the number of bits allocated for each channel of image pixel data. For an RGB-image this will always be 8(bit), while multispectral images can have a larger range of intensity values (e.g. 16bit or 32bit). The `channel` parameter specifies the amount of different image channels and assigns each channel a qualified name and an RGBA-value that indicates the color of the emitted light. This metadata is required for the correct visual representation of multispectral images, but might also have an impact on image analysis methods.

Fluorescence images should always be obtained in `tiff` format to make sure the image data is not mutated by format restrictions (e.g. `png` or `jpeg`):

```python
# regions endpoint
region_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/0/start/0/0/size/1024/1024?image_format=tiff"
```

Therefore it is possible to specify all, but also only dedicated image channels. Individual channels can be selected with the optional parameter `image_channels`. By default all channels are selected. Below selects only AF555- (Id 0) and DAPI-channel (Id 2) for the above given slide info:

```python
# regions endpoint
region_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/0/start/0/0/size/1024/1024?image_format=tiff&image_channels=0&image_channels=2
```

?> For viewing purposes it is also possible to obtain multi-channel images as `png` or `jpeg`, but keep in mind, that all image channels are combined to an RGB-image by averaging the channel intensities. This will **not** be a valid reproduction of the image.

## WSIs with Z Coordinate

If the `z` property of the response of `slide_info_endpoint` is greater than `1`, the `z` coordinate can be specified as query parameter:

```python
url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{size_x}/{size_y}?z=2"
url = f"{APP_API}/v0/{JOB_ID}/tiles/{wsi_id}/level/{level}/position/{pos_x}/{pos_y}?z=2"
```

Resp. in python:

```python
# regions endpoint
Image.open(BytesIO.content))
wsi_id = my_wsi["id"] # see endpoint_1 above
region_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/0/start/0/0/size/1024/1024"
params = {"z": 1}
r = requests.get(tile_url, headers=HEADERS, params=params)
img = Image.open(BytesIO(r.content))
```

!> At the current stage, this feature is not yet implemented.

## Supported WSI Formats

Currently the following WSI formats are supported:

- Hamamatsu (`.ndpi`)
- 3DHistech (`.mrxs`)
- Leica/Aperio (`.svs` / `.scn`)
- Ventana (`.bif`)
- Generic tiff (`.tif`/`.tiff`)
- OME-tif (`.ome.tif` at the moment only fluorescence)
- Philips (`.isyntax`)
- DICOM (`.dcm` files in directory per WSI)
