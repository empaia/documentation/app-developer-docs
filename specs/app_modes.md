# App Modes

Starting with EAD v3, apps can now implement different processing modes. All data structures, that an app either reads as input or generates as outputs, are defined in the `io` section of the EAD. The `modes` section contains one or more modes, that the app implements, by referring to the data structures from the `io` section as mode specific `inputs` and `outputs`.

The following rules apply:

* each data structure in the `io` section must be used at least once as a mode input or output.
* each mode input or output must be specified in the `io` section.
* data structures defined in the `io` section can be reused across modes:
    * e.g. outputs of preprocessing could be inputs of postprocessing.
* when running an app, the mode must be specified in the job object (default: `standalone`).
* an app implementing multiple modes must detect the mode via the `$(EMPAIA_APP_API)/{job_id}/mode` route.

# Mode: Standalone

The `standalone` mode offers a workflow, that is initiated by the user. A user opens the App UI and is presented with a selection / visualization of slides, that are present in the current case. They can then interact with the App UI by drawing a RoI on a slide. The App UI creates a job of type standalone to process the given RoI in the backend. The app is being executed in a container to process inputs and generate new outputs. The App UI polls for the state of the job and visualizes the outputs once the job completed.

# Mode: Preprocessing

The `preprocessing` mode offers the possibility to process complete slides triggered by a slide digitization pipline or the upload of a slide into a cloud environment, depending on the platform implementation and deployment. Time-consuming image processing can be done before a user interacts with a case and preprocessing outputs can be visualized as soon as the user opens the App UI. Users of an organization can define preprocessing triggers, that run jobs for a certain app, if a new slide is ingested into the Platform or clinical system. The preprocessing only works if the desired app explicitily implements this mode, the slides are tagged with both stain and tissue, preprocessing triggers (combination of stain, tissue, app) are defined beforehand and preprocessing requests are created for new slides.

# Mode: Postprocessing

The `postprocessing` mode offers a workflow, that is a combination of an automatic preprocessing and a user initiated postprocessing. Fore example, an app can detect hotspot regions on a complete slide during preprocessing. As soon as a user opens the App UI, preprocessing results are being presented immediately. The workflow provided by the App UI might allow a user to modify the hotspot regions, either by defining additional hotspots or by removing hotspots previously detected by the app. Based on this user interaction, the App UI can run a postprocessing job to calculate scores and similar results across the selected hotspots. Depending on the complexity of such a postprocessing, the postprocessing either can be handled by the containerized app running in a compute cluser or can be calculated by the App UI. If the App UI performs the postprocessing, this must be indicated in the EAD using the `containerized: false` flag of the postprocessing mode section. In this case, job inputs and outputs are set the App UI and can be modified as long as the job is not yet finalized.

# Mode: Report

The `report` mode offers the possibility for an app to select the most important diagnostic results. In the future, these report outputs will be transferred into clinical information systems as part of a larger case report. Therefore a report should only contain the most important results (e.g. tumor scores per RoI and aggregated scores) and not large collections of annotations or classes. If this mode is specified in the EAD, the App UI can create exactly one report job per scope, that summarizes the diagnostic process. While the user interacts with the App UI, this report can be updated continuously in the background (e.g. to update aggregated score, when user adds or removes RoIs). The report job cannot be finalized explicitily, but will the finalized as soon as the corresponding examination is being closed by the user. The job does not run as a containerized app, similar to the postprocessing mode if containerized is set to false.

# Modes Example EAD

```json
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Modes Example",
    "name_short": "Modes",
    "namespace": "org.empaia.vendor_name.modes_example.v3.0",
    "description": "Human readable description",
    "classes": {
        "tumor": {
            "name": "Tumor"
        },
        "non_tumor": {
            "name": "Non tumor"
        }
    },
    "io": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangle": {
            "type": "rectangle",
            "reference": "io.my_wsi"
        },
        "my_cells": {
            "type": "collection",
            "items": {
                "type": "polygon",
                "reference": "io.my_wsi"
            }
        },
        "my_cell_classes": {
            "type": "collection",
            "items": {
                "type": "class",
                "reference": "io.my_cells.items"
            }
        },
        "tumor_ratio": {
            "type": "float",
            "reference": "io.my_rectangle"
        }
    },
    "modes": {
        "standalone": {
            "inputs": [
                "my_wsi",
                "my_rectangle"
            ],
            "outputs": [
                "my_cells",
                "my_cell_classes",
                "tumor_ratio"
            ]
        },
        "preprocessing": {
            "inputs": [
                "my_wsi"
            ],
            "outputs": [
                "my_cells",
                "my_cell_classes"
            ]
        },
        "postprocessing": {
            "containerized": false,
            "inputs": [
                "my_wsi",
                "my_rectangle",
                "my_cells",
                "my_cell_classes"
            ],
            "outputs": [
                "tumor_ratio"
            ]
        },
        "report": {
            "inputs": [],
            "outputs": [
                "tumor_ratio"
            ]
        }
    }
}
```
