# Pixelmaps Level and Resolution Info

The following section covers the rendering of pixelmaps in case the referenced WSI cannot be accessed. To render a pixelmap information about level and resolution of the WSI might be required. Primarily the information about level and resolution for WSIs is provided by a specific endpoint in both APIs. However there can be edge cases, where this endpoint or the information is not accessible (stricter authorization or the WSI was deleted).

## Requesting Pixelmaps Level and Resolution Info

To address this issue, the App Service API and Workbench Service Scopes API both provide an endpoint to request level and resolution information concerning the WSI referenced by a specific pixelmap (`GET /pixelmaps/{pixelmap_id}/info`). The response is structured as followed:

```json
{
  "extent": {
    "x": 10000,
    "y": 10000,
    "z": 0
  },
  "num_levels": 1,
  "pixel_size_nm": {
    "x": 499,
    "y": 499,
    "z": 0
  },
  "levels": [
    {
      "extent": {
        "x": 10000,
        "y": 10000,
        "z": 0
      },
      "downsample_factor": ´1
    }
  ]
}
```

With the provided information an App UI can render a pixelmap without having access to the referenced WSI.

For more information about Pixelmaps Level and Resolution Info see [EMP-0108](https://gitlab.com/empaia/emps/-/blob/main/emp-0108.md)
