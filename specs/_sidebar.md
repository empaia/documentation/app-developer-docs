<!-- docs/_sidebar.md -->

* [Introduction]()
* [Getting started](getting_started)
* [Tutorial: Backend](tutorial_backend/tutorial)
* [Tutorial: Frontend](tutorial_frontend/tutorial)
* [Specs](specs/specs)
  * [EMPAIA App Description](specs/ead)
  * [App Modes](specs/app_modes)
  * [Purpose of References](specs/references)
  * [WSI Data](specs/wsi_data)
  * [Annotation Resolution](specs/annotation_resolution)
  * [Pixelmaps Tile Transfer Compression](specs/pixelmap_tile_compression)
  * [Pixelmaps Level and Resolution Info](specs/pixelmap_resolution)
  * [App API](specs/app_api)
  * [App Requirements](specs/app_requirements)
  * [Workbench API](specs/workbench_api)
  * [App UI Requirements](specs/app_ui_requirements)
* [EMPAIA App Test Suite (EATS)](eats/eats)
* [Best Practices](best_practices)
* [Publish](publish)
* [Glossary](glossary)
