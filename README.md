# App Developer Documentation

Welcome to the EMPAIA App Developer Docs. The following guides provide you with the required information to create, describe and publish your data processing applications (apps) for histology, in a way that they are compatible with the EMPAIA Platform.

To gain a better understanding of the infrastructure provided by EMPAIA, please take a look at the documents linked under [References](#references). Afterwards we suggest to read through the [Getting started](getting_started.md#) page.

App developers can check the API compliance of their apps using the [EMPAIA App Test Suite (EATS)](eats/eats.md#).

## Community

* Join the [EMPAIA Community Group](https://groups.google.com/g/empaia/) for discussions and announcements
* Help us improve the EMPAIA specifications and guidelines with [EMPAIA Mod Proposals (EMP)](https://gitlab.com/empaia/emps/-/blob/main/emp-0001.md)

## Contact

* Contact us directly via [support@empaia.org](mailto:support@empaia.org)

## References

### Publications

* [EMPAIA App Interface: An open and vendor-neutral interface for AI applications in pathology](https://doi.org/10.1016/j.cmpb.2021.106596)
* [The vendor-agnostic EMPAIA platform for integrating AI applications into digital pathology infrastructures](https://doi.org/10.1016/j.future.2022.10.025)

### Open Source Repositories

* [App Developer Docs](https://gitlab.com/empaia/documentation/app-developer-docs/)
* [EMPAIA App Test Suite](https://gitlab.com/empaia/integration/empaia-app-test-suite/)
* [EMPAIA Platform Components](https://gitlab.com/empaia/)

### EMPAIA Platform Components

Detailed overview of the EMPAIA Platform and its components.

<img src="images/detailed_apis.svg"></img>

### Presentation Slides

* <a href="resources/2022-06-01_App_Development.pdf" download>2022-06-01_App_Development.pdf</a>

## Change Log

### 2024-08-28

* EATS 3.7.0 released
  * implemented Pixelmaps Tile Transfer Compression (see [Workbench API: Scope](specs/workbench_api.md#scope), [App API](specs/app_api.md) and [EMP-0107](https://gitlab.com/empaia/emps/-/blob/main/emp-0107.md))
  * implemented Pixelmaps Level and Resolution Info (see [Workbench API: Scope](specs/workbench_api.md#scope), [App API](specs/app_api.md) and [EMP-0108](https://gitlab.com/empaia/emps/-/blob/main/emp-0108.md))
  * added compression to persisted pixelmap tile data
  * updated all services
* **Important:** If you have used a previous version of EATS you have to delete old Docker volumes before starting the services (`eats services down -v`)

### 2024-07-11

* EATS 3.6.1 released
  * extended Workbench v3 Scopes API to support Multi-User Data Sharing in App UI Scopes (see [Workbench API: Scope](specs/workbench_api.md#scope) and [EMP-0104](https://gitlab.com/empaia/emps/-/blob/main/emp-0104.md))
  * updated all services
* **Important:** If you have used a previous version of EATS you have to delete old Docker volumes before starting the services (`eats services down -v`)

### 2024-04-12

* EATS 3.6.0 released
  * changed the CLI for mounting the WSI data directory when starting the services (see [Start services](/eats/basic_usage.md#start-services))
  * the special treatment of multi-file WSI as job inputs has been removed
  * performance improvements in Workbench API
  * updated all services
* **Important:** If you have used a previous version of EATS you have to delete old Docker volumes before starting the services (`eats services down -v`)

### 2024-01-03

* EATS 3.5.1 released
  * the Python dependency has been increased to version >= 3.10

### 2023-12-15

* EATS 3.5.0 released
  * added support for new pixelmap data types
    * for detailed information about the data type definition and possible use cases see [EMP-0102](https://gitlab.com/empaia/emps/-/blob/main/emp-0102.md)
    * added new pixelmap tutorial app (see [Tutorial-Backend](/tutorial_backend/pixelmaps.md))
    * updated *Generic App UI* for pixelmap support (see [Generic App UI](/eats/generic_app_ui.md#pixelmaps))
    * updated API specs
    * updated all services
    * **Important**: Currently the EATS only support apps creating pixelmaps as output of jobs
* **Important:** If you have used a previous version of EATS you have to delete old Docker volumes before starting the services (`eats services down -v`)

### 2023-11-24

* EATS 3.4.6 released
  * fixed error with multi-file WSIs in combination with slide download functionality (see [Multi-file WSIs](/eats/advanced.md#multi-file-wsis))
* **Important:** If you have used a previous version of EATS you have to delete old Docker Volumes before starting the services (`eats services down -v`)

### 2023-11-03

* Improved documentation
  * Separate and updated Best Practices section
  * Documented multi-user testing for App UIs

### 2023-10-26

* EATS 3.4.4 released
  * updated all services
  * minor update of *Generic App UI* for development of research only apps
* **Important:** If you have used a previous version of EATS you have to delete old Docker Volumes before starting the services (`eats services down -v`)

### 2023-09-29

* EATS 3.4.1 released
  * updated all services
    * performance improvements for data queries
    * added multi-user support
      * combined queries for creators **and** jobs are no longer possible in the Workbench Scope API
  * minor update of *Generic App UI* for development of research only apps
* **Important:** If you have used a previous version of EATS you have to delete old Docker Volumes before starting the services (`eats services down -v`)

### 2023-07-19

* EATS 3.3.0 released
  * updated all services
    * performance improvements when finalizing jobs for containerized apps
    * performance improvements when extending collections
    * rework of data queries in Workbench v3 Scopes API
    * performance improvements in Workbench Client 3.0
  * major update of *Generic App UI* for development of research only apps
  * minor changes to Workbench v3 Scopes API (see [Workbench API](specs/workbench_api.md#annotation-queries))
    * removed filter for annotations without referencing class
    * removed filter for annotations not locked for any job
* **Important:** If you have used a previous version of EATS you have to delete old Docker Volumes before starting the services (`eats services down -v`)

### 2023-05-06

* increased frontend token expiration time for App UIs, because browser caching does not work with strict CSP (see [App UI Requirements](specs/app_ui_requirements.md#))

### 2023-04-17

* EATS 3.2.0 released
  * updated all services
  * added a native MIRAX Plugin to access MIRAX WSIs
    * see [MIRAX Plugin](eats/basic_usage.md#start-services) for more information
* **Important:** If you have used a previous version of EATS you have to delete old Docker Volumes before starting the services (`eats services down -v`)

### 2023-03-31

* EATS 3.1.0 released
  * updated all services
  * introduced performance optimizations concerning preprocessing apps
  * updated *Generic App UI* for development or research only apps
* **Important:** If you have used a previous version of EATS you have to delete old Docker Volumes before starting the services (`eats services down -v`)

### 2023-02-16

* Announcing the [EMPAIA Community Group](https://groups.google.com/g/empaia/)

### 2022-11-30

* Platform Release v3
  * AI apps may implement different execution [modes](specs/app_modes), including whole slide preprocessing
  * App UIs may generate reports
  * Workbench Client 3.0 supports a Generic App UI for test and research purposes
  * Support for coexisting API versions
  * Improved data read-write performance of the platform reference implementation
* EATS 3.0.0 released
* Added link to new publication
* **Important:** If you have used a previous version of EATS you have to delete old Docker Volumes before starting the services (`eats services down -v`)
