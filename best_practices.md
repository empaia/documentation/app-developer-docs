# Best Practices

## Backend

* The Medical Data Service regions endpoint serves regions up to a limit of 5000 x 5000 px. If a region of interest (RoI) provided by the user as input exceeds this size limit, the app should attempt to split up the RoI into smaller subregions for memory efficient API queries and processing.
* If possible avoid the Medical Data Service regions entirely and use the tiles endpoint instead. Tiles can be directly streamed from the underlying WSI file format in a lossless and efficient way, although it depends on the implementation details of the plugins used in the Medical Data Service if tile extraction is lossless.
* Developing an app that relies on downloading a complete WSI via `GET /slides/{wsi_id}/download` is **not recommended**, as this may not be supported in all environments. Whether the download is possible can be checked using the `raw_download` field of the slide info response. An app that requires the entire WSI for processing should therefore use the endpoint to access the tiles or use the tile endpoint as a fallback if the download is not supported.
* The Medical Data Service serves WSI layers as they are retrieved from the file using various image format libraries. The app should check which layer has the closest npp (nanometers per pixel) resolution, that is appropriate for the image processing model.
* If no layer has an appropriate resolution, consider scaling the image regions/tiles (preferably scaling down from a higher resolution).
* If some preconditions for a successful processing are not met (e.g. selected RoI too small), use the failure endpoint to gracefully end the processing. The failure endpoint allows the app to send a short but useful message to the user (length limit is imposed by API), that should be as descriptive as possible (e.g. "RoI size of 200 x 100 pixels provided as input is too small. Size must be at least 500 x 500 pixels.").
* The app should be tested with as many different WSIs created by different scanners as possible.
* The app should be tested with various sizes of RoIs and should cover realistic use cases, that pathologists expect to work.
* The usablity should be tested in the Workbench Client. If for example the rendering performance of the viewer is slow, consider specifying a stricter [npp_viewing](specs/annotation_resolution.md#) range for the annotations. If you are using polygons, also consider reducing the number of points per polygon.
* The app must not hardcode a specific npp resolution value for output annotations (e.g. the npp value given in the examples). Instead obtain the actual npp value of the given WSI layer provided by the App API and use it to calculate the correct annotation npp.
* Apps may use Nvidia GPUs with CUDA support for acceleration.
* An app should be programmed to be as memory and time efficient as possible.
* An app should never become stuck in an endless loop, blocking the limited compute resources for other jobs.
* Properly test the App using the EMPAIA App Test Suite. Ensure that the [eats jobs inspect](eats/running_apps.md#inspect-a-job) command does not show any errors and all status fields are `COMPLETED`, otherwise the App does not work as intended.

## Frontend

* Test [multiuser](eats/advanced.md#multi-user-support) behavior.
* Test [read-only mode](tutorial_frontend/workbench_api_usage.md#read-only-mode).
* The usablity should be tested in the App UI. If for example the rendering performance of the viewer is slow, consider specifying a stricter [npp_viewing](specs/annotation_resolution.md#) range for the annotations. If you are using polygons, also consider reducing the number of points per polygon.
