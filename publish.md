# Publish

To publish your app you have to provide the following resources:

* App container image saved as `app-image.tar.gz` file
* EMPAIA App Description (EAD) as `ead.json` file
* Optional: app configuration parameters (defined in EAD) as `config.json` file
* Optional: App UI static resources (e.g. index.html) saved as `app-ui.zip` file

For testing purposes you should also provide anonymized example WSIs and inputs to the EMPAIA Development Team.

## Containerized App Example

This section will show, how an application could be wrapped into a docker container.

The example describes how to containerize a Python app. For other programming languages, e.g. C++, Java or .Net, the project structure and Dockerfile have to be adapted accordingly.

### Project structure

Assuming your application is written in Python, you might have an `app.py` file, with your apps main code, and some mechanism to specify needed packages and versions. For simplicity we chose a simple `requirements.txt` here. To containerize your app you need a `Dockerfile`. All three files are supposed to be in your apps root folder:

```
containerized_app_example/
  ├── app.py
  ├── requirements.txt
  └── Dockerfile
```

`app.py`:

For simplicity, and as you might already be familiar with that app, we take the [Tutorial Simple App](tutorial_backend/simple_app.md#) as example.

`requirements.txt`:

```
Pillow==7.2.0
requests==2.24.0
```

`Dockerfile`:

```Dockerfile
FROM python:3.6
ADD . /
RUN pip install -r requirements.txt
ENTRYPOINT python3 -u /app.py
```

Build the container image:

```bash
docker build -t sample-app .
```

Save the container image as a tarball:

```bash
docker save sample-app | gzip > app-image.tar.gz
```

## App UI Example

The App UI must be submitted as pre-built static files, not the raw source code of your JavaScript Framework. The following directory structure serves as an example:

```
app_ui_bundle/
├── assets/
│   └── fonts/
│       └── example-font.woff2
├── index.html
├── main.js
├── runtime.js
├── polyfills.js
└── styles.css
```

Zip only the contents of `app_ui_bundle`, such that the `app_ui_bundle` itself is not contained. The file should be named `app_ui_bundle.zip`.

If your App UI requires an app ui configuration, this file must also be provided and should be named `app_ui_config_file.json`.

## Testing

Test your app in the [EMPAIA App Test Suite](eats/eats.md#) before submitting the app. Ensure that the [eats jobs inspect](eats/running_apps.md#inspect-a-job) command does not show any errors and all status fields are `COMPLETED`, otherwise the App does not work as intended.

## Submit your App

Contact [support@empaia.org](mailto:support@empaia.org) on how to submit your app.
