# Getting Started

EMPAIA offers integration paths for commercial and research use cases.

## Commercial

Commercial vendors can offer their image processing and AI apps to clinics and pathology laboratories, that are part of the EMPAIA ecosystem, via a central marketplace. Allowing pathologists to use these apps in their routine workflows requires vendors to adhere to certain medical software regulations. Therefore, AI vendors provide their data processing algorithms in the form of a containerized app and in addition provide an App UI in the form of a browser-based web frontend (single-page application). Apps with custom App UIs give vendors full control over the diagnostic workflow presented to users, which in turn is an important aspect in guaranteeing end-to-end conformity with regulatory requirements.

In order to become involved in the EMPAIA ecosystem, please contact us about a potential partnership at [dev-support@empaia.org](mailto:dev-support@empaia.org).

If you want to begin the development of a commercial app, we advise you to start with the [Backend Tutorial](tutorial_backend/tutorial.md#), where the integration of data processing applications with the EMPAIA App API is described. For more background information, please refer to the <a href="https://www.sciencedirect.com/science/article/pii/S0169260721006702">EMPAIA App Interface publication</a>. When the backend implementation is finished, you can start integrating your App UI with the EMPAIA Workbench API as described in the [Frontend Tutorial](tutorial_frontend/tutorial.md#). You can test your App and App UI with the [EMPAIA App Test Suite (EATS)](eats/eats.md#).

## Research

EMPAIA explicitly supports research use cases. In a future update of the EMPAIA marketplace it will be possible to offer such apps to a wide audience, as long as the apps are properly marked as "for research only". Implementing an EMPAIA compatible research app is already possible via the [EMPAIA App Test Suite (EATS)](eats/eats.md#). Many researchers might want to focus on image processing algorithms and not on the diagnostic workflow. In this case it is possible to only implement an app following the [Backend Tutorial](tutorial_backend/tutorial.md#). Apps without UI can use a Generic App UI with limited functionality, that is included in the EATS.

Of course it is also possible for research apps to implement a custom App UI, as described in the [commercial apps section](#commercial).
