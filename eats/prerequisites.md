# Prerequisites

EATS 2.0 can be used on Linux, Windows and Mac. Please follow the specific guidelines for your operating system. All EATS tutorial sections use `bash` commands.

## Ubuntu 22.04

EATS 2.0 supports [Docker Engine](https://docs.docker.com/engine/install/) on Ubuntu 22.04. Other Linux distributions may work as well, but have not been tested. Alternatives like podman or outdated Docker versions from other package repositories are not supported.

Install Docker:

* [Docker Engine for Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
* For Cuda GPU support also install the [NVIDIA Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/index.html)

Install packages:

```bash
sudo apt update
sudo apt install git python3-venv
```

Optional: add your Ubuntu user to the `docker` group to access the docker-engine without `sudo` and restart your operating system afterwards:

```bash
sudo usermod -a -G docker <username>
```

Check if docker is working:

```bash
docker run --rm hello-world
```

## Windows

EATS 2.0 supports the usage of WSL 2 (Windows Subsystem for Linux) with Docker for Windows. Powershell support is not available anymore.

Install and configure the following components:

* [Windows Terminal](https://www.microsoft.com/de-de/p/windows-terminal/9n0dx20hk701)
* [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/)
* [Ubuntu 22.04 on WSL](https://www.microsoft.com/en-us/p/ubuntu/9nblggh4msv6) with [upgrade to WSL2](https://docs.microsoft.com/en-us/windows/wsl/install#upgrade-version-from-wsl-1-to-wsl-2)
* [Docker Desktop for Windows](https://docs.docker.com/desktop/windows/install/) with [WSL 2 backend](https://docs.docker.com/desktop/windows/wsl/)
* Integration of Docker Desktop for Windows with Ubuntu 20.04 on WSL 2 [enabled](https://docs.docker.com/desktop/windows/wsl/#install)

Open WSL 2 shell in Windows Terminal and install Python 3:

```bash
sudo apt update
sudo apt install git python3-venv
```

Optional: add your Ubuntu user to the `docker` group to access the docker-engine without `sudo` and open a new shell afterwards:

```bash
sudo usermod -a -G docker <username>
```

Check if docker is accessible from WSL 2:

```bash
docker run --rm hello-world
```

## macOS

EATS 2.0 supports [Docker Engine](https://docs.docker.com/engine/install/) with Docker for Mac. It was tested with macOS Big Sur on a Mac with an Intel chip. Other versions that support Docker Desktop for Mac, namely macOS Catalina and macOS Monterey, might also work, but have not yet been tested. The same applies to Macs with Apple chips.

?> Currently, Safari is not fully compatible with all features of the Workbench Client 3.0. Please use another browser, e.g. Firefox, to test your App UI.

Install Docker:

* [Install Docker Desktop on Mac](https://docs.docker.com/desktop/mac/install/)

Install packages:

There are many ways to install git, python3 and python3-venv on macOS. We would recommend to install and use [Homebrew](https://brew.sh/index_de). Afterwards run the following commands:

```bash
brew install git python3
pip3 install virtualenv
```

Check if docker is working:

```bash
docker run --rm hello-world
```
