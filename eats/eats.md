# EMPAIA App Test Suite (EATS)

The *EMPAIA App Test Suite (EATS)* is designed to help you in the development process of creating a containerized app and a custom App UI for the EMPAIA ecosystem.

With the EATS it is possible to check if

* the syntax of your app's EAD is correct
* the requests of your containerized app against the App API are correct
* the requests of your App UI against the Workbench Scope API are correct
* the user experience is satisfactory

To test your containerized app during development the EATS provides several CLI commands to register your app in the EMPAIA platform, create and execute jobs and export job results. Testing of your custom App UI can be done using the Workbench Client 3.0.

Once the App UI is functional you can start new jobs in the Workbench Client 3.0, view results of previously run jobs and test the postprocessing functionality in your App UI if your app supports this mode.
