# Basic Usage

This section guides you through some remaining steps to set up your test environment and demonstrates basic commands to use the EMPAIA App Test Suite.

## Download Test Slide

Download a sample WSI from the [Openslide WSI repository](http://openslide.cs.cmu.edu/download/openslide-testdata/Aperio/CMU-1.svs) and store it at some path **/SOME/PATH/TO/WSIS/CMU-1.svs**.

## Download Sample Apps

Download or clone the [Sample Apps repository](https://gitlab.com/empaia/integration/sample-apps), known from the tutorial:

```bash
git clone https://gitlab.com/empaia/integration/sample-apps.git
```

For all proceeding instructions we assume the following folder structure. Also, interesting tutorial apps for the Test Suite are shown in the folder structure:

```
eats/
├── .venv/
├── dist/
├── licenses/
└── sample-apps/
    └── sample_apps/
        ├── test/
        │   └── internal/
        │       ├── _ITA05_input_templates/
        │       │   └── v3/
        │       └── _ITA06_nested_large_collections/
        │           └── v3/
        └── tutorial/
            ├── _TA01_simple_app/
            ├── _TA02_collections_and_references/
            ├── _TA03_annotation_results/
            ├── _TA04_output_references_output/
            ├── _TA05_classes/
            ├── _TA06_large_collections/
            ├── _TA07_configuration/
            ├── _TA08_failure_endpoint/
            ├── _TA09_additional_inputs/
            ├── _TA10_app_with_ui/
            ├── _TA11_preprocessing/
            └── _TA12_containerized_postprocessing/
```

## Help Command

For information about the CLI usage and arguments execute:

```bash
eats --help
```

For more information on each sub command, e.g.:

```bash
eats jobs --help
eats jobs register --help
```

## Start services

Start the services with:

```bash
eats services up </SOME/PATH/TO/WSIS/>
```

?> **/SOME/PATH/TO/WSIS/** must be the absolute path of the directory where the WSI **CMU-1.svs** is located. This directory will be mounted as a data directory (**/data**) in all relevant service containers (Clinical Data Service and WSI format plugins).

?> The very first run of the `eats services up` command requires some time, as quite a few background services need to be pulled or build as docker images.

?> You can activate isyntax image file support by running `eats services up` with the flag `--isyntax-sdk <path/to/philips-pathologysdk-2.0-ubuntu18_04_py36_research.zip>` once. To get the SDK you have to register and download it from the Philips Pathology SDK Site (Note: Make sure to download the version for Ubuntu 18.04 and Python 3.6.9).

?> You can activate a native support for MIRAX WSI files by running `eats services up` with the flag `--mirax-plugin <path/to/mirax-backend.zip>` once. You will need to build the required backend yourself on a Windows PC. A build guide can be found here: [Mirax Backend](https://gitlab.com/empaia/integration/mirax-backend). Please note, that the EATS supports MIRAX WSI files via Openslide. If you encounter a problem when using MIRAX WSIs, you can use the plugin to test whether it can fix this problem. The MIRAX plugin has not been tested extensively yet.

Let us take a look at the inputs for our app:

```bash
cd sample-apps/sample_apps/tutorial/_TA01_simple_app/v3/
code ./eats_inputs/my_wsi.json
cd -
```

```JSON
{
    "type": "wsi",
    "path": "/data/CMU-1.svs",
    "id": "3e86a882-bb3d-4d2f-b31d-c5704ba3efc1",
    "tissue": "PROSTATE",
    "stain": "H_AND_E"
}
```

No need to edit anything here, just note that `"path"` contains our mount point. If more WSIs reside in `/SOME/PATH/TO/WSIS` or a subdirectory, they can be specified as well.

?> The `"path"` can also be specified relative to the data directory **/data**. The relative path for this example would be: `"path": "CMU-1.svs"`.

?> The keys `"tissue"` and `"stain"` are optional. If either or both are omitted, `null` (JSON response in API request) or `None` (e.g. as output of the `eats slides list` command) will be used as default values. Valid values for `"tissue"` and `"stain"` are listed on GitLab in the [Definitions repository](https://gitlab.com/empaia/integration/definitions/-/blob/main/tags/tags.csv). The supported values for tissue (`TAG_GROUP` = `TISSUE`) or stain (`TAG_GROUP` = `STAIN`) are the entries in column `TAG_NAME_ID`.

?> In case of multi-file WSIs the `"path"` parameter specifies the main file of the WSI (e.g. for MIRAX this is the `.mrxs` file). All secondary files will be discoverd automatically.

## Stop Services and Delete Jobs

It is possible to register and run multiple jobs without restarting backend services. The services can be stopped, if they are not needed anymore.

```bash
eats services down
```

To erase all data (registered WSIs, registered apps and jobs, etc.) use the `-v` switch:

```bash
eats services down -v
```
