<!-- docs/_sidebar.md -->

* [Introduction]()
* [Getting started](getting_started)
* [Tutorial: Backend](tutorial_backend/tutorial)
* [Tutorial: Frontend](tutorial_frontend/tutorial)
* [Specs](specs/specs)
* [EMPAIA App Test Suite (EATS)](eats/eats)
  * [Prerequisites](eats/prerequisites)
  * [Installation](eats/installation)
  * [Basic Usage](eats/basic_usage)
  * [Running Apps](eats/running_apps)
  * [App UI URL](eats/app_ui_url)
  * [Generic App UI](eats/generic_app_ui)
  * [Containerized Postprocessing](eats/postprocessing)
  * [Advanced Usage](eats/advanced)
* [Best Practices](best_practices)
* [Publish](publish)
* [Glossary](glossary)
