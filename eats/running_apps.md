# Running Apps

This section will guide you through using the EATS with the sample app from the [Tutorial: Frontend](tutorial_frontend/tutorial.md#), i.e. _TA10_app_with_ui.

Afterwards you will have executed every step necessary to test your own App and App UI.

## Prepare App

First we need to build the docker image for the example app of the tutorial:

```bash
cd sample-apps/sample_apps/tutorial/_TA10_app_with_ui/v3/
docker build -t ta10 .
```

## Register App

Next we need to register our app at our locally running Marketplace-Service. The command to register the example app requires the following parameters:

* the docker image name: `ta10`
* the path to the ead: `ead.json`
* the URL to the App UI: `--app-ui-url http://host.docker.internal:8888/sample-app-ui`

By default we deploy a very strict Content Security Policy for App UIs. Since the Sample App UI of the tutorial app requires more relaxed security settings, we need to provide an additional app ui configuration file, to enable these security policy settings. This can be done using the `--app-ui-config-file <path/to/app-ui-config-file.json>` flag.

The `app_ui_config.json` for the tutorial app is provided within the app directory:

```JSON
{
    "csp": {
        "style_src": {
            "unsafe_inline": true
        },
        "font_src": {
            "unsafe_inline": true
        }
    }
}
```

The App UI is built using the Angular framework. Angular requires the CSP setting `style_src: unsafe_inline`. The setting `font_src: unsafe_inline` is required by the way fonts are handled in the tutorial App UI.

?> For more information about the Content Security Policy and required App UI config settings see [Web Security](specs/app_ui_requirements.md#web_security)

EATS 3.0 ships the sample-app-ui using a container. Please note, that this is only for demonstration purposes. App UIs will not be published in the form of container images. For more information refer to the [publish](publish.md#) section.

The Sample App UI is served through an NGINX reverse proxy. The address on localhost is `http://localhost:8888/sample-app-ui`. When the WBC loads the App UI, it does not access it directly through the `localhost` address. Instead it requests the App UI resources through the Workbench v3 Frontends API. The Workbench Service has access to the host network via `host.docker.internal`. Therefore the address translates to `http://host.docker.internal:8888/sample-app-ui` for the `--app-ui-url` setting. For more information refer to the [App UI URL](eats/app_ui_url.md#) section.

!> If you are using a different port for the NGINX reverse proxy, the port in the URL must be adapted accordingly.

```bash
eats apps register ead.json ta10 --app-ui-url http://host.docker.internal:8888/sample-app-ui --app-ui-config-file app_ui_config.json > app.env
```

?> If your app requires configuration parameters, `--global-config-file <path/to/global-config.json>` and / or `--customer-config-file <path/to/customer-config.json>` must be appended.

The above command will create the file `app.env` containing an `APP_ID`, which can be stored as an environment variable for later use with:

```bash
export $(xargs < app.env)
echo $APP_ID
```

To view all registered apps use:

```bash
eats apps list
```

## Register and Run Job

A new job is registered by using the `APP_ID` and providing a folder containing the job inputs. This will produce the environment variables for your app (`EMPAIA_JOB_ID`, `EMPAIA_TOKEN`, `EMPAIA_APP_API`) that will be saved to `job.env`.

The provided input folder must contain a JSON file for each required job input with the same name as the input declared in the EAD. In this case, we need the files `slide.json` and `region_of_interest.json`.

The input `region_of_interest` has a class contraint declared in the EAD:

```json
"region_of_interest": {
    "type": "rectangle",
    "reference": "io.slide",
    "classes": [
        "org.empaia.global.v1.classes.roi"
    ]
}
```

Therefore it is required to provide a class with the value `org.empaia.global.v1.classes.roi` as aditional input in the input folder. In this example the folder `eats_inputs` contains the file `rois.json`:

```json
{
    "item_type": "class",
    "items": [
        {
            "value": "org.empaia.global.v1.classes.roi",
            "reference_id": "42b2403d-c5f7-4f0d-a4e6-cb8ecc5ecc35", # id of 'region_of_interest'
            "reference_type": "annotation"
        }
    ]
}
```

?> Multiple classes or other class values, e.g. classes declared in the app namespace within the EAD, can also be defined. The name of the file containing these class constraints does not matter as long as it is a valid JSON file and has the extension `.json`. See [Classes as Input](eats/advanced.md#classes-as-input) for additional information.

```bash
eats jobs register $APP_ID ./eats_inputs > job.env
```

?> When a job is registered, all required inputs from `./eats_inputs` (except WSIs) are created and persisted in the **Medical Data Service**. Implicitly a case, an examination and a scope are created as well. The ID of the created scope is used as the creator of all created inputs.

Run the app, providing the file including the environment variables `job.env`:

```bash
eats jobs run ./job.env
```

?> A registered job can only be run once. Starting the job a second time will result in an error.

## Job Status

The job ID can be retrieved from the `job.env` file to be used in other commands.

```bash
export $(xargs < job.env)
echo $EMPAIA_JOB_ID
```

Regularly check the job's status until it is `COMPLETED`.

?> Depending on the elapsed time, the job will be in one of these states: `SCHEDULED`, `RUNNING` or `COMPLETED`. If the status is `ERROR`, the job did not complete successfully. See [Debugging](/eats/advanced.md#debugging) for help. Checking the status of a job before starting it will result in the status `ASSEMBLY`.

```bash
eats jobs status $EMPAIA_JOB_ID
```

## Wait for Job

A job has completely finished its execution if it is in a finished state. Finished states are:

* `COMPLETED`: the job execution ended without any error
* `ERROR`: the job execution ended with a call of `PUT /failure`
* `FAILED`: the job execution ended with an unhandled error and was therefore terminated by the **Job Execution Service**
* `TIMEOUT`: the job execution exceeded the time limit given by the **Job Execution Service**

Additionally all inputs and outputs of a job are validated for correctness regarding the EAD.

Since jobs could have a long execution time, it might not be practical to repeatedly use the `eats jobs status` command. Instead you can use

```bash
eats jobs wait $EMPAIA_JOB_ID
```

The `eats jobs wait` command will periodically check the status of the job execution and validation. The command returns either if the execution and validation are in the state `COMPLETED` or if any error has occurred.

## Inspect a Job

To get detailed information about a job or access error messages if the job execution or validation has failed, you can use

```bash
eats jobs inspect $EMPAIA_JOB_ID
```

Make sure to check the `input_validation_status` and `output_validation_status` fields. These checks are running asynchronously and do not block the job processing, but should switch into status `COMPLETED` eventually. Otherwise an error message will inform you about any problems with the data inputs and outputs of your App.

Example of the resulting output:

```json
{
    "app_id": "d8b730fc-5e5f-4064-8b0b-f7c802c0aadf",
    "creator_id": "997b9413-438d-4d54-a759-4b49d6109fba",
    "creator_type": "SCOPE",
    "mode": "STANDALONE",
    "containerized": true,
    "id": "a9dec1e4-8eac-4768-bc72-917ff603fe2b",
    "inputs": {
        "slide": "a7df6b17-3747-4820-bc3b-87a4b6151526",
        "region_of_interest": "e3be03ae-e3df-445e-9dee-010a4d3c62fe"
    },
    "outputs": {
        "positivity": "36135623-3606-4327-8c4c-4b664de92ae2",
        "detected_nuclei": "ac133663-200b-41f2-bfbc-a0961ddf0a8d",
        "number_negative": "d627e6ed-7b0f-4d08-a5b4-186e52a64b34",
        "number_positive": "05d1a542-6224-4e97-ae17-00fb4c8f64a7",
        "model_confidences": "c14e7d99-79d1-48ba-affa-1b48e7c83371",
        "nucleus_classifications": "357e4d77-8871-4235-b989-06dc1fac2716"
    },
    "status": "COMPLETED",
    "created_at": 1680076622,
    "started_at": null,
    "progress": null,
    "ended_at": 1680076672,
    "runtime": null,
    "error_message": null,
    "input_validation_status": "COMPLETED",
    "input_validation_error_message": null,
    "output_validation_status": "COMPLETED",
    "output_validation_error_message": null
}
```

## Results - JSON

To export the outputs of the job:

```bash
eats jobs export $EMPAIA_JOB_ID ./job-outputs
```

## Results - Workbench Client

Open http://localhost:8888/wbc3/ in a Browser to review the job results using the EMPAIA Workbench Client 3.0.

!> Up to step 3 the navigation is the same for all apps. From step 4 onward each App UI has its own workflow and the navigation is different. Below the navigation of the Sample App UI from the [Tutorial: Frontend](tutorial_frontend/tutorial.md#) is demonstrated.

For simplicity, all slides are added to a static single case. Navigate to:

1. **Cases** and select the case:

  <img src="images/app_test_suite/wbc3/01_wbc3_case.png"></img>

1. **Apps** and select your app:

  <img src="images/app_test_suite/wbc3/02_wbc3_app.png"></img>

1. Click **Start**:

  <img src="images/app_test_suite/wbc3/03_wbc3_app_start.png"></img>

1. **App UI Specific Navigation**
   1. **Slides** and select the slide:

     <img src="images/app_test_suite/wbc3/04_wbc3_slide.png"></img>

   1. View the results, e.g.
       * The app took one RoI *REGION_OF_INTEREST* as input
         * The green check mark indicates a successful job execution
       * The App generated three outputs referencing the input RoI
         * *number positive* as an integer value
         * *number negative* as an integer value
         * *positivity* as a float value
         * **Note:** the order of the results is based on their respective ids and can be different for each displayed job

     <img src="images/app_test_suite/wbc3/05_wbc3_results.png"></img>

   1. Navigate in the viewer to see the point annotations generated by the app

     <img src="images/app_test_suite/wbc3/06_wbc3_annots.png"></img>

   1. Choose a different color palette to increase contrast and visibility of annotations for different stains and tissue types. Additionally the stroke width (i.e. line thickness) of the annotations can be adjusted if needed.

     <img src="images/app_test_suite/wbc3/07_wbc3_color.png"></img>

   1. If multiple jobs were run you can hide all results or only specific job results (check box beside input RoI - not shown in picture)

     <img src="images/app_test_suite/wbc3/08_wbc3_hide.png"></img>
