# Advanced Usage

This section contains information on further features of the EATS, e.g. executing your app from within the Workbench Client, just like a user would do it, or features to help debugging your app.

## Changing Ports

By default the EATS exposes only a single port (default: 8888) to the host system. This is the port of the nginx reverse proxy server. If you need to change that port you can use an optional parameter when starting the EATS services:

```bash
eats services up --nginx-port 9999 ./wsi-mount-points.json
```

!> If you change the default port you must adapt the shown example commands in the previous sections accordingly!

## Accessing OpenAPI Docs

When the EATS is running you can access the OpenAPI Documentation of some services:

Relevant APIs for Development:

* Workbench Service (v3 Scopes API): `http://localhost:8888/wbs-api/v3/scopes/docs`
* App Service: `http://localhost:8888/app-api/v3/docs`

Additional APIs:

* Workbench Service (General API): `http://localhost:8888/wbs-api/docs`
* Workbench Service (v3 API): `http://localhost:8888/wbs-api/v3/docs`
* Workbench Service (v3 Frontends API): `http://localhost:8888/wbs-api/v3/frontends/docs`
* Job Execution Service: `http://localhost:8888/jes-api/docs`
* Medical Data Service: `http://localhost:8888/mds-api/v3/docs`

!> If you have changed the default port you must adapt the port in the above URLs accordingly!

## Changing Token Expiration Times

During the development process it can be useful to set longer expiration times for tokens used in the EATS and by an App UI.

To change the expiration time for one or both token types use the following parameters when starting the EATS services:

* `--scope-token-exp {time in seconds}`: set the expiration time of scope tokens in seconds (default: 300)
* `--frontend-token-exp {time in seconds}`: set the expiration time of frontend tokens in seconds (default: 60)

!> Never expect long expiration times of tokens in your App UI! Your App UI must be able to refresh a token when the current token is expired!

## Update App UI and configuration

If you registered your app using `eats apps register` you might have omitted optional parameters to supply an App UI url, an App UI configuration and / or the global / customer configuration. If you want to set one or more of these settings or change previously supplied settings you can use the `eats apps update $APP_ID` command. This command acepts the same optional parameters as the `eats apps register` command:

* `--global-config-file <path/to/global-config.json>`
* `--customer-config-file <path/to/customer-config.json>`
* `--app-ui-url <app-ui-url>`
* `--app-ui-config-file <path/to/app-ui-config-file.json>`

## Enable GPU support

In order to run a Docker Image with GPU support, (a) the host is required to have appropriate GPU capabilities, and (b) the Docker containers have to be started with specific parameters in order to request that GPU support. Requesting GPU support when none is available results in an error.

Thus, by default, if the parameter is not provided, the Job Execution Service within the EATS does not make use of GPUs. To activate and request GPU support for each started app, you have to specify the GPU driver when starting the EATS, using the optional `--gpu-driver` parameter.

```bash
eats services up --gpu-driver nvidia ./wsi-mount-points.json
```

Here, `--gpu-driver nvidia` is used to enable CUDA support. The values are not defined and evaluated by the EATS but passed directly to Docker when starting the app images. Please refer to the Docker documentation for other possible values. If the parameter is not provided, GPU support is disabled.

At the moment, the EMPAIA compute environment is set up to support CUDA-enabled -- i.e. `nvidia` -- GPUs, on systems with a suitable version of the [NVIDIA Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/) installed. Other GPU drivers may work if they are installed on the host system, but are not yet officially supported. The NVIDIA Container Toolkit allows apps running inside containers to access the GPUs of the host machine. **Note:** Currently this is only available for selected Linux distributions and container runtimes. Compatibility and installation information can be found in [NVIDIA's official documentation](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html).

Note that GPU support will not be available in every compute environment. To ensure maximum compatibility, it is recommended to implement a fallback such that the app works both with and without GPU support. In case the app is not able to function without GPUs, it should check for the availability of GPU support and exit with an appropriate status code and message if none are available.

## WSIs in DICOM format

In the EATS WSIs in DICOM format are supported. We recommend using [wsidicomizer](https://pypi.org/project/wsidicomizer/) to convert supported WSIs to DICOM. Other converters have not been extesively tested / are not supported yet.

When definining your WSIs as job inputs you have to use the directory containing the `.dcm` files as `"path"`:

```JSON
{
    "type": "wsi",
    "path": "/data/dicom_directory",
    "id": "37bd11b8-3995-4377-bf57-e718e797d515"
}
```

The key `"id"` is optional and only needed for references (see [References](eats/advanced.md#references))

## Running an App using the Workbench Client 3.0

All slides, that have been used as job inputs (via `eats jobs register`), will be available in the Workbench Client 3.0 in one case.
If you want to add further slides, without registering jobs via CLI, you can use the following command:

```bash
eats slides register <path-to-wsi-input.json>
```

Also, all apps, that have been registered (via `eats apps register`), will be available to be executed as new jobs within the Workbench Client 3.0 under **Apps**. For this purpose, select an app and start a new job from within the App UI (**Generic App UI** if selected app was registered without specifying an UI).

## Delete slide

To test whether the App UI can handle deleted slides, a slide can be deleted manually via the command line:

```bash
eats slides delete <slide_id>
```

?> Please note: The slide is only marked as deleted in the database but will not be removed from the file system.

## Multi-User support

The Workbench Service's Scopes API is designed to enable App UIs to allow users to collaborate with their colleagues, such as sharing results of completed jobs. Every authenticated user can see and inspect all cases, slides and examinations in the Workbench Client 3.0. As soon as an App is selected, it is determined by the App UI, which data is available to a specific user. The API used by App UIs to create and retrieve data is the Scopes API and all endpoints are bound to a Scope tied to the current user in the context of an examination and App (for more information about Scopes see: [Scopes API](specs/workbench_api.md#scopes-api)). All data created via the Scopes API is implicitly created in the context of the Scope (the ID of the Scope is used as the creator of new data). If the Scopes API is used to request data (e.g. jobs, annotations or data collections), various filters are available to retrieve only the relevant data items (see [Workbench API](specs/workbench_api.md#) for available query filters). In most queries the parameter `creators` can be used to filter the requested data by a given list of creator IDs. In the Scopes API only the scope ID of the current user is allowed as a value for this parameter. Therefore only data created by that user is returned. Therefore, to implement multi-user support in App UIs, the `creators` filter should not be used in the vast majority of API requests.

?> There is one exception, where data created by a specific user (scope) is not visible to other users: Jobs that are not yet started (jobs still in the state `ASSEMBLY`) are only visible for the user who has created them.

To test the multi-user support of your app UI, jobs can be registered with the additional CLI flag `--alt-user`. If this CLI flag is used, a different scope (for a different user ID) is used to create the job and its inputs.

```bash
eats jobs register $APP_ID ./eats_inputs --alt-user > job.env
```

Additionaly, there is an second Workbench Client 3.0 available, which uses the alternative user ID as its default. To access the second Workbench Client 3.0 you can open `localhost:8888/wbc3-alt-user` in your browser in parallel to the default Workbench Client 3.0 (`localhost:8888/wbc3`).

?> If you are using a different port for the NGINX reverse proxy, the port in the URL must be adapted accordingly.

## Debugging

The job ID is used as the container name. It can be used to retrieve docker logs:

```bash
docker logs ${EMPAIA_JOB_ID}
```

If a job is taking too long or is stuck, the job can be aborted:

```bash
eats jobs abort ${EMPAIA_JOB_ID}
```

To inspect backend service logs the `docker logs` command can be used directly. The names of all service containers can be retrieved using the `eats services containers` command. For example, we can retrieve the logs of the `app-service` to see the requests our app has sent to the API:

```bash
eats services list  # print list of service names
docker logs app-service
```

## References

The `id` property in input files for an app does not necessarily have to be present. But when an input is used as reference for another input, the `id` **must** be set (uuid4). For example, take a look at `my_wsi.json` and `my_rectangle.json` inside `sample-apps/sample_apps/valid/tutorial/_01_simple_app/eats_inputs`:

`my_wsi.json` (see `id`):

```JSON
{
    "type": "wsi",
    "path": "/data/CMU-1.svs",
    "id": "3e86a882-bb3d-4d2f-b31d-c5704ba3efc1",
    "tissue": "PROSTATE",
    "stain": "H_AND_E"
}
```

`my_rectangle.json` (see `reference_id`)

```JSON
{
    "type": "rectangle",
    "upper_left": [
        1000,
        2000
    ],
    "width": 300,
    "height": 500,
    "reference_id": "3e86a882-bb3d-4d2f-b31d-c5704ba3efc1",
    "npp_created": 499,
    "npp_viewing": [1, 499123]
}
```

!> When defining an input collection, either all items and the collection itself must have an `id` or none at all.

!> When modifying an input JSON-file with `id` property set, you either have to delete your previously stored data (`docker volume rm $(eats services volumes)`) or assign a new `id` if you want to keep your previous jobs including all data.

## Classes as Input

It might happen that some input annotations in your EAD have a `classes` constraint, but the classes themselves are not an input parameter. In this case, the classes still have to be defined as input JSON-files. For reference, see the property `inputs.my_rectangles.items.classes` in `sample-apps/sample_apps/tutorial/_TA05_classes/v3/ead.json`:

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Tutorial App 05 v3",
    "name_short": "TA05v3",
    "namespace": "org.empaia.vendor_name.tutorial_app_05.v3.0",
    "description": "Human readable description",
    "classes": {
        "tumor": {
            "name": "Tumor"
        },
        "non_tumor": {
            "name": "Non tumor"
        }
    },
    "io": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangles": {
            "type": "collection",
            "items": {
                "type": "rectangle",
                "reference": "io.my_wsi",
                "classes": [
                    "org.empaia.global.v1.classes.roi"
                ]
            }
        },
        "my_cells": {
            "type": "collection",
            "items": {
                "type": "collection",
                "reference": "io.my_rectangles.items",
                "items": {
                    "type": "point",
                    "reference": "io.my_wsi"
                }
            }
        },
        "my_cell_classes": {
            "type": "collection",
            "items": {
                "type": "collection",
                "items": {
                    "type": "class",
                    "reference": "io.my_cells.items.items"
                }
            }
        }
    },
    "modes": {
        "standalone": {
            "inputs": [
                "my_wsi",
                "my_rectangles"
            ],
            "outputs": [
                "my_cells",
                "my_cell_classes"
            ]
        }
    }
}
```

The corresponding input directory `/sample-apps/sample_apps/tutorial/_TA05_classes/v3/eats_inputs` contains the files `my_wsi.json`, `my_rectangles.json`, but also a file `rois.json`:

```JSON
{
    "item_type": "class",
    "items": [
        {
            "value": "org.empaia.global.v1.classes.roi",
            "reference_id": "73621cf4-1c2d-44fd-99fc-4937afda8519",
            "reference_type": "annotation",
        },
        {
            "value": "org.empaia.global.v1.classes.roi",
            "reference_id": "0fb368f6-9345-405a-83af-ab4d38141cd5",
            "reference_type": "annotation",
        },
        {
            "value": "org.empaia.global.v1.classes.roi",
            "reference_id": "2026af14-9108-49dc-992d-6eb4315f789e",
            "reference_type": "annotation",
        }
    ]
}
```

## Configuration Parameter

If your app contains configuration parameters, have a look at the `ead.json` and `eats_inputs/configuration/configuration.json` under `sample-apps/sample_apps/tutorial/_TA07_configuration/v3/`. To register the job, add the argument `--global-config-file` and / or `--customer-config-file`:

```bash
cd ../_07_configuration
docker build -t app07 .
eats apps register TA07 ead.json --global-config-file ./eats_inputs/configuration/configuration.json
# or
eats apps register TA07 ead.json --customer-config-file ./eats_inputs/configuration/customer_configuration.json
# or
eats apps register TA07 ead.json --global-config-file ./eats_inputs/configuration/configuration.json --customer-config-file ./eats_inputs/configuration/customer_configuration.json
```

?> Note: The configuration JSON-file **must not** reside directly in the inputs' directory. At least a subdirectory should be used.

## App Inputs and Outputs

In order to write the required `.json` files to specify the input of your app, please refer to the content of the tutorial apps `sample-apps/sample_apps/tutorial/` and their corresponding input files.

An example app with all different kinds of input variables, can be found under `sample-apps/sample_apps/test/internal/_ITA05_input_templates/v3/`.

## Executing Your App without Job Execution Service

The command `eats jobs run $EMPAIA_JOB_ID <job.env-file>` sends a request to the Job Execution Service to start your containerized app. Debugging your app this way can be tedious and time-consuming. To speed up the debugging process, you can also start your app natively:

1. Start services as usual:

    ```bash
    eats services up ./wsi-mount-points.json
    ```

2. Register app as usual, receiving the `APP_ID`

    ```bash
    eats apps register <path-to-ead.json> <docker-image-name> > app.env
    export $(xargs < app.env)
    echo $APP_ID
    ```

3. Register job and export to `job.env`, e.g.

    ```bash
    eats jobs register $APP_ID <eats-inputs-folder> > job.env
    export $(xargs < job.env)
    echo $EMPAIA_JOB_ID
    ```

4. Overwrite the `EMPAIA_APP_API` value with `localhost:<NGINX-PORT>/app-api` (the exposed port of the NGINX proxy server is `8888` by default):

    ```bash
    export EMPAIA_APP_API=http://localhost:8888/app-api
    ```

5. Set job to status `RUNNING` (without actually running it):

    ```bash
    eats jobs set-running $EMPAIA_JOB_ID
    ```

6. Run your app, e.g.:

    ```bash
    python3 <path-to-app.py>
    ```

7. Make changes to your app.py and **GO TO 3.**

## Proxy Configuration

In case you are using the EATS behind a proxy, make sure your docker proxy-configuration is set correctly.

1. Navigate to `.docker` in your user directory

    ```bash
    cd ~/.docker
    ```

2. Exclude all used EMPAIA services in your `config.json` as defined below:

    ```JSON
    {
        "proxies": {
            "default": {
                "httpProxy": "http://proxy.example.de:8080",
                "httpsProxy": "http://proxy.example.de:8080",
                "noProxy": "*.example.de,127.0.0.1,localhost,app-service,annotation-service,job-service,examination-service,clinical-data-service,medical-data-service,marketplace-service-mock,aaa-service-mock,job-execution-service,workbench-daemon,workbench-service,workbench-client-v3,workbench-client-v3-sample-ui,workbench-client-v3-generic-app-ui,nginx,eats-postgres-db,eats-mongo-db,host.docker.internal"
            }
        }
    }
    ```
