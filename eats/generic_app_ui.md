# Generic App UI

It may be desirable to test your containerized app before your App UI is entirely implemented or integrated. For this purpose, the EATS provides a *Generic App UI* that supports a variety of different EAD variants (see below). Another use case for the *Generic App UI* are pure research apps that do not require a certified or app-specific UI.

If you register your app without explicitly specifying an App UI via the `--app-ui-url` parameter, the *Generic App UI* is used by default in the Workbench Client 3.0.

The *Generic App UI* supports Apps with app modes **preprocessing** or **standalone**. If the EAD of an App declares the app mode **preprocessing**, the app mode **standalone** will not be supported by the UI. If you want to use the *Generic App UI* for the **standalone** app mode, there must not be a **preprocessing** app mode in the EAD.

!> A complete App for the EMPAIA Platform should always include an app-specific UI to support all needed input and output data for your app.

## Preprocessing mode

If the *Generic App UI* is used in the **preprocessing** mode, only apps with a single WSI as input are supported.

?> The *Generic App UI* can be used to view the results of **preprocessing** jobs. The jobs must be registered and started via the CLI.

Steps to use the *Generic App UI*:

1. Selecting a preprocessing app in the app panel

<img src="images/app_test_suite/generic_app_ui_preprocessing_1.png"></img>

2. Select a slide

<img src="images/app_test_suite/generic_app_ui_preprocessing_2.png"></img>

3. The *Generic App UI* automatically queries all available preprocessing jobs

<img src="images/app_test_suite/generic_app_ui_preprocessing_3.png"></img>

In the result panel, primitive results, referencing the slide, will be shown in the lower segment. When an annotation is clicked in the viewer, the results view is updated to show the primitive results associated with the selected annotation (Note that the used app does not create such an output).

The icon to the right of each job indicates its current status. In the screenshot the indicator shows a job where the execution and also the validation of the inputs and outputs was sucessful. There are special indicators if a job execution terminated with an error, or the validation is still running or resulted in an error. Moving the mouse cursor over an indicator will reveal more information.

## Standalone mode

The **standalone** mode of the *Generic App UI* supports a single WSI and different types of ROIs as inputs. A ROI can be a circle, rectangle or polygon annotation.

Supported ROIs in the *Generic App UI* are:

* a single ROI **or**
* one or more collection(s) of ROIs
  * only one collection of each annotation type is supported
  * only unnested collections are supported

For the Generic App UI to correctly assign a ROI class to job input annotations it is required to use a ROI classes constraint in the EAD as shown in [Tutorial: Backend - Classes](tutorial_backend/classes.md#ead) (also see EAD snippet below). If this constraint is not set and the ROI classes are not assigned, the annotations will not match the `classes` filter in the annotations viewer query and therefore will not be rendered in the viewer.

EAD snippet:

```json
"classes": [
    "org.empaia.global.v1.classes.roi"
]
```

?> When the job is started via the CLI, any number and combination of ROIs can be used as input. However, the display of inputs in the job result overview section may be incomplete (in the viewer all annotations should always be visible).

### Single ROI

Register your App and a valid slide via the CLI and then open the Workbench Client 3.0.

1. Open the case from the case list, click **+ NEW** in the **APPS** panel and select your registered App from the **ADD NEW APP** panel

<img src="images/app_test_suite/generic_app_ui_standalone_single_roi_1.png"></img>

2. Open the *Generic App UI* and select a slide

<img src="images/app_test_suite/generic_app_ui_standalone_single_roi_2.png"></img>

3. Select the supported geometric RoI type from the drawing tools at the top of the *Generic App UI*

<img src="images/app_test_suite/generic_app_ui_standalone_single_roi_3.png"></img>

4. Draw a RoI and assign a name. Click **Save** to start the job

<img src="images/app_test_suite/generic_app_ui_standalone_single_roi_4.png"></img>

5. A spinning circle beside the RoI indicates a running job in the **REGION OF INTEREST** panel on the right side

<img src="images/app_test_suite/generic_app_ui_standalone_single_roi_5.png"></img>

6. When the job execution has finished, the spinning circle is replaced by a green check mark

<img src="images/app_test_suite/generic_app_ui_standalone_single_roi_6.png"></img>

7. You can click the input RoI or any of the output annotations in the slide viewer to view primitive results referencing the selected element in the **Primitive Result** panel on the lower right side

<img src="images/app_test_suite/generic_app_ui_standalone_single_roi_7.png"></img>

### Multi ROI

Register your App and a valid slide via the CLI and then open the Workbench Client 3.0.

1. Open the case from the case list, click **+ NEW** in the **APPS** panel and select your registered App from the **ADD NEW APP** panel

<img src="images/app_test_suite/generic_app_ui_standalone_multi_roi_1.png"></img>

2. Open the *Generic App UI* and select a slide

<img src="images/app_test_suite/generic_app_ui_standalone_multi_roi_2.png"></img>

3. To create a new job, click on **+** in the **JOBS** panel on the right side

<img src="images/app_test_suite/generic_app_ui_standalone_multi_roi_3.png"></img>

4. Select a supported geometric RoI type from the drawing tools at the top of the *Generic App UI*

<img src="images/app_test_suite/generic_app_ui_standalone_multi_roi_4.png"></img>

5. Draw at least one RoI and assign a name. To start the execution click the **PLAY** button next to the job

<img src="images/app_test_suite/generic_app_ui_standalone_multi_roi_5.png"></img>

6. A spinning circle indicates a running job in the **JOBS** panel

<img src="images/app_test_suite/generic_app_ui_standalone_multi_roi_6.png"></img>

7. When the job execution has finished, the spinning circle is replaced by a green check mark

<img src="images/app_test_suite/generic_app_ui_standalone_multi_roi_7.png"></img>

8. You can click an input RoI or any of the output annotations in the slide viewer to view primitive results referencing the selected element in the **Primitive Result** panel on the lower right side

<img src="images/app_test_suite/generic_app_ui_standalone_multi_roi_8.png"></img>

### Pixelmaps

Precondition: Run an app, that creates one or more pixelmaps.

1. Select case, app and slide. In the **Results** section on the lower right side all pixelmaps are listed as soon as the results for the slide are shown (the pixelmap, in this example referred to as **Segmentation Map**, is created as part of the job output).

<img src="images/app_test_suite/generic_app_ui_pixelmap_1.png"></img>

2. Click on any pixelmap to select it for visualization. In the **Visualization** section on the upper right side the class color mapping for the selected pixelmap is displayed. All classes defined in the `element_class_mapping` are shown here. The class **Background** used the `neutral_value` specified in the pixelmap metadata and therefore doesn't have an assinged color and will be transparent in the viewer.

<img src="images/app_test_suite/generic_app_ui_pixelmap_2.png"></img>

3. Zoom into a region of the WSI in which tissue is located to see the pixelmap data in the viewer. You can select different color maps in the **Visualization** section or change the oppacity for each channel of the pixelmap individually in the upper tool pannel.

<img src="images/app_test_suite/generic_app_ui_pixelmap_3.png"></img>
