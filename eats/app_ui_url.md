# App UI URL

When testing an App with an appropriate app-specific App UI within the EATS, consider the following:

* Your App UI must be reachable by the Workbench Service, from within the `workbench-service` container
* Your App UI must be built according to the [Tutorial: Frontend](tutorial_frontend/tutorial)

Furthermore, due to the internal architecture of the EATS, there are additional steps to be taken. The following diagram illustrates the integration of the App UI into the Workbench Client 3.0:

<img src="images/app_test_suite/eats_networking.svg"></img>

As shown in the diagram the Workbench Service (WBS) runs as a docker container in the docker network *EATS*. To be able to reach your locally served App UI resources the WBS must contact the host system from within the docker network. In order to make this possible, it is necessary that:

* your locally served App UI is bound to host `0.0.0.0`
* your locally served App UI allows the host `host.docker.internal`

The following command serves as an example:

```bash
nx serve --host 0.0.0.0 --port 4200 --allowed-hosts host.docker.internal
```

For the example given in the diagram, the resulting **App UI URL** to use with `--app-ui-url` when registering your app would be: `http://host.docker.internal:4200`.

?> If no App UI URL is specified with `--app-ui-url` when registering your App the *Generic App UI* will be used in the Workbench Client 3.0. More information about the *Generic App UI* is available here: [*Generic App UI*](eats/generic_app_ui)

## Disable Content Security Policies

For App UI development, it may be helpful if certain security features of the Workbench Client could be temporarily bypassed. The Content Security Policies can be disabled when starting the EATS services using the optional `--app-ui-disable-csp` parameter in the `eats services up` command. If you use this option, an App UI config file will have no effect and can be omitted.

?> If the Content Security Policies are disabled the optional parameter `--app-ui-connect-src` and `--app-ui-frame-ancestors` are forbidden.

!> Always test your App UI with Content Security Policies **enabled** and use an appropriate App UI config file if necessary.
