# Installation

Create a virtual environment and activate it (recommended but not required):

```bash
python3 -m venv .venv
source .venv/bin/activate
```

Install EATS from PyPI (Python Package Index):

```bash
pip install empaia-app-test-suite
```

?> On WSL 2 the `pip install` might take longer than usual, if the `.venv` is located in a Windows directory. The general performance is not heavily impacted by this.

## Installation of Test Releases on Test PyPI

If you want to use not yet officially available releases of the EATS to use upcoming new features you can install these test releases from Test PyPI. Create a virtual environment and activate it as shown above and change the `pip install` command (`-i https://test.pypi.org/simple/` tells `pip` to install the package from test.pypi.org, `--extra-index-url https://pypi.org/simple/` ensures that the dependencies are installed from pypi.org):

```bash
pip install -i https://test.pypi.org/simple/ --extra-index-url https://pypi.org/simple/ empaia-app-test-suite
```

!> All apps must always be tested with the current, official release of the EATS before being submitted.
