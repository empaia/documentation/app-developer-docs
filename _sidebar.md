<!-- docs/_sidebar.md -->

* [Introduction]()
* [Getting started](getting_started)
* [Tutorial: Backend](tutorial_backend/tutorial)
* [Tutorial: Frontend](tutorial_frontend/tutorial)
* [Specs](specs/specs)
* [EMPAIA App Test Suite (EATS)](eats/eats)
* [Best Practices](best_practices)
* [Publish](publish)
* [Glossary](glossary)
