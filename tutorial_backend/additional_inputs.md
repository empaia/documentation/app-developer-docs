# Additional Inputs

All Tutorial Apps shown so far are very simple and only accept a single WSI and a single rectangle or a rectangle collection as inputs. This is mainly due to the fact that complex inputs and outputs are difficult to map with a generic app UI. However, apps with a custom UI are not restricted by those limitations as long as they are consuming and producing data that is supported by the Workbench API.

For this example we will consider an app, that processes multiple RoIs on multiple WSIs. As results, the app produces a cell count for each processed RoI and calculates an avarage cell count for each processed WSI. The processing of each RoI is parameterized by a float value, that serves as a threshold.

In this section we will:

* Demonstrate the usage of more complex inputs
* Introduce references on multiple levels in nested collections

!> The presented app currently only works with the EATS CLI commands. Due to the more complex inputs the Generic App UI of the Workbench Client 3.0 is not able to set the necessary inputs. Therefore the job can't be started in the Workbench Client 3.0. After a job is completed the Generic App UI can only visualize the input RoIs on each WSI, primitive data types (job outputs) are not supported. The Generic App UI will receive additional features in upcoming EATS releases and will support more complex input and output data.

## Inputs

* 1 Collection of WSIs (key: `my_wsis`).
* 1 Nested collection of many rectangle annotations (key: `my_rectangles`) specifying the regions to be processed on each slide:
  * Each inner collection references one of the input WSIs in `my_wsis`:
    * Each rectangle annotation references one of the input WSIs in `my_wsis` (the same WSI that includes the collection).
* 1 Nested collection of many float primitives (key: `my_thresholds`) specifying threshold values:
  * Each float primitive references one of the rectangle annotations in `my_rectangles`.

## Outputs

* 1 Nested collection of many tumor cell counts (key: `tumor_cell_counts`), where each resulting count references a rectangle annotation (from `my_rectangles`).
* 1 Collection of average tumor cell counts (key: `avg_tumor_cell_counts`), computed across all rectangle annotations on one WSI, therefore referencing a collection (the items of the input collection `my_rectangles`).

## EAD

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Tutorial App 09 v3",
    "name_short": "TA09v3",
    "namespace": "org.empaia.vendor_name.tutorial_app_09.v3.0",
    "description": "Human readable description",
    "io": {
        "my_wsis": {
            "type": "collection",
            "items": {
                "type": "wsi"
            }
        },
        "my_rectangles": {
            "type": "collection",
            "items": {
                "type": "collection",
                "reference": "io.my_wsis.items",
                "items": {
                    "type": "rectangle",
                    "reference": "io.my_wsis.items"
                }
            }
        },
        "my_thresholds": {
            "type": "collection",
            "items": {
                "type": "collection",
                "items": {
                    "type": "float",
                    "reference": "io.my_rectangles.items.items"
                }
            }
        },
        "tumor_cell_counts": {
            "type": "collection",
            "items": {
                "type": "collection",
                "items": {
                    "type": "integer",
                    "reference": "io.my_rectangles.items.items"
                }
            }
        },
        "avg_tumor_cell_counts": {
            "type": "collection",
            "items": {
                "type": "float",
                "reference": "io.my_rectangles.items"
            }
        }
    },
    "modes": {
        "standalone": {
            "inputs": [
                "my_wsis",
                "my_rectangles",
                "my_thresholds"
            ],
            "outputs": [
                "tumor_cell_counts",
                "avg_tumor_cell_counts"
            ]
        }
    }
}
```

* Parameters of `"type": "collection"` must include an `"items"` property to specify the type of items contained inside the collection. You may nest collections (`type` of `items` is also `"collection"`).
* The reference parameter is mandatory for annotations and classes, but optional for other types. Its effect differs depending on the input or output parameter used.
  * Inputs use references as constraint:
    * Each rectangle annotation of `my_rectangles` has to be an annotation of one of the input WSIs `my_wsis`.
    * Each float primitive of `my_thresholds` must reference one of the rectangle annotations in `my_rectangles`.
  * Outputs use references for semantic purposes:
    * Each integer value of the nested collection `tumor_cell_counts` will refer to one rectangle annotation of `my_rectangles`.
    * Each float value of the collection `avg_tumor_cell_counts` will refer to one of the inner collections of `my_rectangles`.

For more information about how references might be used, have a look at [Purpose of References](specs/references.md#).

## App API Usage

```python
import os
from io import BytesIO

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    ... # see Simple App

def get_input(key: str):
    ... # see Simple App

def post_output(key: str, data: dict):
    ... # see Simple App

def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    ... # see Simple App

def get_processing_data(my_wsi: dict, my_rectangles: dict):
    """
    get wsi rectangle dict
    """
    data = {}
    for wsi in my_wsis["items"]:
        data[wsi["id"]] = get_rectangles_for_wsi(my_rectangles, wsi["id"])
    return data


def get_rectangles_for_wsi(my_rectangles: dict, wsi_id: str):
    """
    get rectangle collection with rectangles referencing given wsi_id
    """
    for rectangle_collection in my_rectangles["items"]:
        if rectangle_collection["reference_id"] == wsi_id:
            return rectangle_collection


def get_threshold_for_rectangle(my_thresholds: dict, rectangle_id: str):
    """
    get threshold for given rectangle_id
    """
    thresholds = {}
    for threshold_collection in my_thresholds["items"]:
        for threshold in threshold_collection["items"]:
            if threshold["reference_id"] == rectangle_id:
                return threshold["value"]
    return None


def count_tumor_cells(wsi_tile: Image, threshold: float):
    """
    pretends to do something useful

    Parameters:
        wsi_tile: WSI image tile
    """
    return int(threshold * 100)


def compute_avg(tumor_cell_counts: dict):
    """
    pretends to do something useful

    Parameters:
        tumor_cell_counts: contains list of items with numeric value
    """
    values = [item["value"] for item in tumor_cell_counts["items"]]
    return sum(values) / len(values)


my_wsis = get_input("my_wsis")
my_rectangles = get_input("my_rectangles")
my_thresholds = get_input("my_thresholds")

tumor_cell_counts = {
    "item_type": "collection",
    "items": [],
    "type": "collection",  # NEW required in v3 apps
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}
avg_tumor_cell_counts = {
    "item_type": "float",
    "items": [],
    "type": "collection",  # NEW required in v3 apps
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

data = get_processing_data(my_wsis, my_rectangles)

for wsi_id in data:
    rectangles = data[wsi_id]["items"]

    wsi_tumor_cell_counts = {
        "item_type": "integer",
        "items": [],
        "type": "collection",  # NEW required in v3 apps
        "creator_type": "job",  # NEW required in v3 apps
        "creator_id": JOB_ID,  # NEW required in v3 apps
    }

    for rectangle in rectangles:
        wsi_tile = get_wsi_tile(wsi_id, rectangle)

        tumor_cell_count = {
            "name": "cell count tumor",  # choose name freely
            "type": "integer",
            "value": count_tumor_cells(wsi_tile, get_threshold_for_rectangle(my_thresholds, rectangle["id"])),
            "reference_id": rectangle["id"],  # set reference to rectangle
            "reference_type": "annotation",
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        }
        wsi_tumor_cell_counts["items"].append(tumor_cell_count)

    wsi_avg_tumor_cell_count = {
        "name": "avg tumor cell count",
        "type": "float",
        "value": compute_avg(wsi_tumor_cell_counts),
        "reference_id": data[wsi_id]["id"],  # set reference to rectangle collection
        "reference_type": "collection",
        "creator_type": "job",  # NEW required in v3 apps
        "creator_id": JOB_ID,  # NEW required in v3 apps
    }

    tumor_cell_counts["items"].append(wsi_tumor_cell_counts)
    avg_tumor_cell_counts["items"].append(wsi_avg_tumor_cell_count)

post_output("tumor_cell_counts", tumor_cell_counts)
post_output("avg_tumor_cell_counts", avg_tumor_cell_counts)

put_finalize()
```

## Further Examples

For more examples regarding input types and their declaration in the EAD see [here](https://gitlab.com/empaia/integration/sample-apps/-/blob/master/sample_apps/test/internal/_ITA05_input_templates/v3/ead.json).
