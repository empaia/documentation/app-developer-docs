# Large Collections

Input and output collections can become quite large, eventually too large for one single request. This example shows how to retrieve large input collections in batches, as well as how to post large output collections in batches.

In this section we will:

* Fetch large input collections in batches:
  * Note the `shallow` query parameter to fetch a big collection without an `items` list.
  * Use `skip` and `limit` as query parameters to fetch the collections in batches.
* Show how output collections can be saved in batches.

## App Class Scope

* tumor
* non_tumor

## Inputs

* 1 WSI (key: `my_wsi`).
* 1 Collection of many rectangle annotations (key: `my_rectangles`) specifying the regions to be processed.

## Outputs

* 1 Collection of collections of cell point annotations. Each of the inner collections references one of the input rectangles from the `my_rectangles` collection.

## EAD

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Tutorial App 06 v3",
    "name_short": "TA06v3",
    "namespace": "org.empaia.vendor_name.tutorial_app_06.v3.0",
    "description": "Human readable description",
    "io": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangles": {
            "type": "collection",
            "items": {
                "type": "rectangle",
                "reference": "io.my_wsi"
            }
        },
        "my_cells": {
            "type": "collection",
            "items": {
                "type": "collection",
                "reference": "io.my_rectangles.items",
                "items": {
                    "type": "point",
                    "reference": "io.my_wsi"
                }
            }
        }
    },
    "modes": {
        "standalone": {
            "inputs": [
                "my_wsi",
                "my_rectangles"
            ],
            "outputs": [
                "my_cells"
            ]
        }
    }
}
```

## App API Usage

```python
import os
from io import BytesIO
from random import randint
from typing import List

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    ... # see Simple App

def get_input(key: str, shallow: bool = False):
    ... # see Simple App

def post_output(key: str, data: dict):
    ... # see Simple App

def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    ... # see Simple App

def get_large_collection(key: str):
    """
    fetches a collection in batches
    """
    collection_shallow = get_input(key=key, shallow=True)
    item_count = collection_shallow["item_count"]
    collection_id = collection_shallow["id"]
    skip = 0
    limit = 1000
    while skip < item_count:
        r = requests.put(
            f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/query?skip={skip}&limit={limit}",
            json={},  # different filters possible. We dont want to filter any here.
            headers=HEADERS,
        )
        r.raise_for_status()
        collection_shallow["items"] += r.json()["items"]
        skip += limit
    return collection_shallow


def post_items_to_collection(collection_id: str, items: list):
    """
    add items to an existing output collection

    Parameters:
        items: list of data elements
    """
    url = f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/items"
    r = requests.post(url, json={"items": items}, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def detect_cells(my_wsi: dict, my_rectangle: dict):
    """
    pretends to find cells in a rectangle

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    wsi_tile = get_wsi_tile(my_wsi, my_rectangle)
    _ = wsi_tile  # wsi_tile not used in dummy code below

    x_min = my_rectangle["upper_left"][0]
    x_max = my_rectangle["upper_left"][0] + my_rectangle["width"]
    y_min = my_rectangle["upper_left"][1]
    y_max = my_rectangle["upper_left"][1] + my_rectangle["height"]
    cells_batch = []
    for _ in range(12345):
        cell = {
            "name": "cell",
            "type": "point",
            "reference_id": my_wsi["id"],  # each point annotation references my_wsi
            "reference_type": "wsi",
            "coordinates": [
                randint(x_min, x_max),
                randint(y_min, y_max),
            ],  # Always use WSI base level coordinates
            "npp_created": 499,  # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
            "npp_viewing": [
                499,
                3992,
            ],  # (optional) recommended pixel reslution range for viewer to display annotation, if npp_created is not sufficient
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        }
        cells_batch.append(cell)

    return cells_batch


my_wsi = get_input("my_wsi")
# get collection in batches
my_rectangles = get_large_collection("my_rectangles")

my_cells = {
    "item_type": "collection",
    "items": [],
    "type": "collection",  # NEW required in v3 apps
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

# prepare and post a shallow nested collection
my_cells = post_output("my_cells", my_cells)

# extend nested shallow collection in batches (size 1000)
for rectangle in my_rectangles["items"]:
    my_cells_inner = [
        {
            "item_type": "point",
            "items": [],
            "reference_id": rectangle["id"],
            "reference_type": "annotation",
            "type": "collection",  # NEW required in v3 apps
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        }
    ]
    my_cells_inner = post_items_to_collection(my_cells["id"], my_cells_inner)
    inner_collection_id = my_cells_inner["items"][0]["id"]  # extended top level collection by one item, hence index 0
    cells = detect_cells(my_wsi, rectangle)
    batch_size = 1000
    start_idx = 0
    end_idx = start_idx + batch_size
    while start_idx < len(cells):
        post_items_to_collection(inner_collection_id, cells[start_idx:end_idx])
        start_idx += batch_size
        end_idx = start_idx + batch_size

put_finalize()
```
