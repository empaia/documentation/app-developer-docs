# Standalone App with UI

In the following example the app the [Frontend Tutorial](tutorial_frontend/tutorial.md#/standalone_with_ui) is based on is briefly presented. If you have worked through the previous examples no new concepts will be shown here.

## App Class Scope

* pos
* neg

## Inputs

* 1 WSI (key: `slide`).
* 1 Rectangle annotation (key: `region_of_interest`) specifying the region to be processed:
  * must have class `org.empaia.global.v1.classes.roi`.

## Outputs

* 1 Point annotation collection (key: `detected_nuclei`), where each result annotation references the input WSI.
* 1 Float primitive collection (key: `model_confidences`), where each result confidence references one of the point annotations.
* 1 Class collection (key: `nucleus_classifications`), where each result class references one of the point annotations.
  * Values for the classes must be declared in `"classes"` within the EAD.
* 1 Integer primitive (key: `number_positive`), references the input rectangle.
* 1 Integer primitive (key: `number_negative`), references the input rectangle.
* 1 Float primitive (key: `positivity`), references the input rectangle.
  
## EAD

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Tutorial App 10 v3",
    "name_short": "TA10v3",
    "namespace": "org.empaia.vendor_name.tutorial_app_10.v3.0",
    "description": "Sample app to demonstrate custom frontend components",
    "classes": {
        "pos": {
            "name": "Positive"
        },
        "neg": {
            "name": "Negative"
        }
    },
    "io": {
        "slide": {
            "type": "wsi"
        },
        "region_of_interest": {
            "type": "rectangle",
            "reference": "io.slide",
            "classes": [
                "org.empaia.global.v1.classes.roi"
            ]
        },
        "detected_nuclei": {
            "type": "collection",
            "reference": "io.region_of_interest",
            "items": {
                "type": "point",
                "reference": "io.slide"
            }
        },
        "model_confidences": {
            "type": "collection",
            "items": {
                "type": "float",
                "reference": "io.detected_nuclei.items"
            }
        },
        "nucleus_classifications": {
            "type": "collection",
            "items": {
                "type": "class",
                "reference": "io.detected_nuclei.items"
            }
        },
        "number_positive": {
            "type": "integer",
            "reference": "io.region_of_interest"
        },
        "number_negative": {
            "type": "integer",
            "reference": "io.region_of_interest"
        },
        "positivity": {
            "type": "float",
            "reference": "io.region_of_interest"
        }
    },
    "modes": {
        "standalone": {
            "inputs": [
                "slide",
                "region_of_interest"
            ],
            "outputs": [
                "detected_nuclei",
                "model_confidences",
                "nucleus_classifications",
                "number_positive",
                "number_negative",
                "positivity"
            ]
        }
    }
}
```
