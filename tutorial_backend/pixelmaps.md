# Pixelmaps

The app in this example only declares a `preprocessing` app mode to create a pixel based tissue and cell nuclei segmentation in form of a nominal pixelmap.

## App Class Scope

* tissue
* cell_nuclei
* background

## Rendering hints

The App uses rendering hints to select suitable colors in order to visualize annotations and nominal pixelmap data of a certain class.

### Annotation rendering

Only the class value for the **tissue** class is listed:

* "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.tissue"

### Nominal Pixelmap rendering

The values for the **tissue** and **cell_nuclei** class are listed:

* "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.tissue"
* "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.cell_nuclei"

## Inputs

* 1 WSI (key: `input_wsi`).

## Outputs

* 1 polygon annotation collection (key: `particles`), where each result annotation encloses a detected tissue particle that references the input WSI.
* 1 class collection (key: `particle_classes`), where each class references a polygon annotation from `particles`.
* 1 integer primitive (key: `number_of_tissue_particles`) referencing the input WSI.
* 1 float primitive (key: `tissue_ratio`) referencing the input WSI.
* 1 nominal pixelmap (key: `tissue_nuclei_map`) referencing the input WSI.

## EAD

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Tutorial App 13 v3",
    "name_short": "TA13v3",
    "namespace": "org.empaia.vendor_name.tutorial_app_13.v3.0",
    "description": "Human readable description",
    "classes": {
        "tissue": {
            "name": "Tissue",
            "description": "Valid tissue particles"
        },
        "cell_nuclei": {
            "name": "Cell nuclei",
            "description": "Segmented cell nuclei"
        },
        "background": {
            "name": "Background",
            "description": "Background"
        }
    },
    "rendering": {
        "annotations": [
            {
                "class_value": "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.tissue",
                "color": "#00FF00",
                "color_hover": "#64FF64",
                "color_selection": "#C8FFC8"
            }
        ],
        "nominal_pixelmaps": [
            {
                "class_value": "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.tissue",
                "color": "#00FF00",
                "color_hover": "#64FF64",
                "color_selection": "#C8FFC8"
            },
            {
                "class_value": "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.cell_nuclei",
                "color": "#FFFF00",
                "color_hover": "#FFFF64",
                "color_selection": "#FFFFC8"
            }
        ]
    },
    "io": {
        "input_wsi": {
            "type": "wsi"
        },
        "particles": {
            "type": "collection",
            "items": {
                "type": "polygon",
                "reference": "io.input_wsi",
                "classes": [
                    "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.tissue"
                ]
            }
        },
        "particle_classes": {
            "type": "collection",
            "items": {
                "type": "class",
                "reference": "io.particles.items"
            }
        },
        "number_of_tissue_particles": {
            "type": "integer",
            "reference": "io.input_wsi"
        },
        "tissue_ratio": {
            "type": "float",
            "reference": "io.input_wsi"
        },
        "tissue_nuclei_map": {
            "type": "nominal_pixelmap",
            "reference": "io.input_wsi",
            "channel_classes": [
                "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.tissue",
                "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.cell_nuclei"
            ],
            "element_classes": [
                "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.background",
                "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.tissue",
                "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.cell_nuclei"
            ]
        }
    },
    "modes": {
        "preprocessing": {
            "inputs": [
                "input_wsi"
            ],
            "outputs": [
                "particles",
                "particle_classes",
                "number_of_tissue_particles",
                "tissue_ratio",
                "tissue_nuclei_map"
            ]
        }
    }
}
```

?> For the nominal pixelmap two class constraints are declared: `channel_classes` and `element_classes`. These constraints can be used to specifically declare class values from the global and app class namespace which are valid values for *channel_class_mapping* or *element_class_mapping* (see the below example on how to set class mappings for pixelmaps). The compliance with these constraints is checked during the output validation.

## App API Usage

Only the general processing workflow and pixelmap specific code is shown here. Image processing code is not shown.

```python
APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}

def put_tile_to_pixelmap(pixelmap_id: str, level: int, tile_x: int, tile_y: int, data: bytes):
    """
    add tile to an existing output pixelmap
    """
    url = f"{APP_API}/v3/{JOB_ID}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data"
    r = requests.put(url, data=data, headers=HEADERS)
    r.raise_for_status()

mode = get_app_mode()["mode"]

# error if app mode not PREPROCESSING
if mode != "PREPROCESSING":
    error_msg = "Invalid job mode!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)

# get input wsi
input_wsi = get_input("input_wsi")
input_wsi_id = input_wsi["id"]

# check wsi meta data -> tiles must be a square and between 256 and 2048
tile_extent_x = input_wsi["tile_extent"]["x"]
tile_extent_y = input_wsi["tile_extent"]["y"]

if tile_extent_x != tile_extent_y:
    error_msg = "Only WSIs supported where tilesize_x == tilesize_y!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)

if tile_extent_x < 256 or tile_extent_x > 2048:
    error_msg = "Only WSIs supported where 256 <= tilesize <= 2048!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)

# get overview image and scaling
overview, scaling = get_overview(input_wsi)

# error if no overview
if not overview or not scaling:
    error_msg = "Overview could not be created!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)

# detect tissue and fetch global thresholds -> result: list of polygons with base layer coordinates, global thresholds
tissue_particles, global_tissue_threshold, global_nuclei_threshold = get_tissue_and_global_thresholds(overview, scaling)

# post output polygon collection
particle_collection = get_empty_collection(item_type="polygon")
particle_collection = post_output("particles", particle_collection)

# post output class collection
particle_class_collection = get_empty_collection(item_type="class")
particle_class_collection = post_output("particle_classes", particle_class_collection)

particle_polygons = get_tissue_particle_polygons(tissue_particles, input_wsi)

# extend output collections with polygons and classes
if len(particle_polygons) > 0:
    particle_items_with_ids = post_items_to_collection(particle_collection["id"], particle_polygons)
    tissue_classes = get_tissue_classes(particle_items_with_ids["items"])
    post_items_to_collection(particle_class_collection["id"], tissue_classes)

# get range of wsi tiles for x and y -> these are the bounds used in the pixelmap level definition
tile_range = get_tile_range(input_wsi, tissue_particles)

# get nominal pixelmap with core properties
pixelmap = get_nominal_pixelmap_core(input_wsi_id)

# set pixelmap metadata
pixelmap["tilesize"] = tile_extent_x  # tilesize equals tilesize of slide
pixelmap["levels"] = [  # pixelmap level: only level for base layer will be created
    {
        "slide_level": 0,
        "position_min_x": tile_range[0],
        "position_min_y": tile_range[1],
        "position_max_x": tile_range[2],
        "position_max_y": tile_range[3],
    }
]
pixelmap["channel_count"] = 2  # channel count: 2 -> tissue and cell nuclei
pixelmap["channel_class_mapping"] = [
    {"number_value": 0, "class_value": TISSUE_CLASS},
    {"number_value": 1, "class_value": CELL_NUCLEI_CLASS},
]
pixelmap["neutral_value"] = 0  # neutral value: 0 -> will be transparent in Generic App UI
pixelmap["element_class_mapping"] = [
    {"number_value": 0, "class_value": BACKGROUND_CLASS},
    {"number_value": 1, "class_value": TISSUE_CLASS},
    {"number_value": 2, "class_value": CELL_NUCLEI_CLASS},
]

# post pixelmap object
pixelmap = post_output("tissue_nuclei_map", pixelmap)

# get base layer tiles completely / partly within particles
tiles_to_process = get_tiles_to_process(tissue_particles, input_wsi)

# process all relevant tiles to create pixelmap data
# for each tile the binary data for the pixelmap tile will be created and posted
for tile in tiles_to_process:
    upper_left = tile.exterior.coords[0]
    x = int(upper_left[0] / tile_extent_x)
    y = int(upper_left[1] / tile_extent_y)
    tile_data = process_tile(input_wsi_id, 0, x, y, global_tissue_threshold, global_nuclei_threshold)
    put_tile_to_pixelmap(pixelmap["id"], 0, x, y, tile_data)

put_finalize()
```
