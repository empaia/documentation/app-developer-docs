# Annotation Results

In order to count tumorous cells, the App needs to detect these cells. The EAD allows the app to upload the cell data as point annotations.

In this section we will:

* Add a collection of collections of annotations as output.

## Inputs

* 1 WSI (key: `my_wsi`).
* 1 Collection of many rectangle annotations (key: `my_rectangles`) specifying the regions to be processed.

## Outputs

* 1 Collection of collections of many tumor cell point annotations (key: `tumor_cells`). Each of the inner collections references one of the input rectangles from the `my_rectangles` collection.

## EAD

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Tutorial App 03 v3",
    "name_short": "TA03v3",
    "namespace": "org.empaia.vendor_name.tutorial_app_03.v3.0",
    "description": "Human readable description",
    "io": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangles": {
            "type": "collection",
            "items": {
                "type": "rectangle",
                "reference": "io.my_wsi"
            }
        },
        "my_cells": {
            "type": "collection",
            "items": {
                "type": "collection",
                "reference": "io.my_rectangles.items",
                "items": {
                    "type": "point",
                    "reference": "io.my_wsi"
                }
            }
        }
    },
    "modes": {
        "standalone": {
            "inputs": [
                "my_wsi",
                "my_rectangles"
            ],
            "outputs": [
                "my_cells"
            ]
        }
    }
}
```

* For each item in `my_rectangles` the app finds multiple cells, hence the `reference` from the inner collection to one rectangle `io.my_rectangles.items`.
* Annotations always reference whole slide images. Therefore each detected cell (point annotation) has a `reference` to `io.my_wsi`.
* For a list of possible reference combinations see [Specs - Data Types and References - References](specs/ead.md#references).
* For a list of available annotation types see  [Specs - Data Types and References - Annotation Data Types](specs/ead.md#annotation-data-types).

## App API Usage

```python
import os
from io import BytesIO

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    ... # see Simple App

def get_input(key: str):
    ... # see Simple App

def post_output(key: str, data: dict):
    ... # see Simple App

def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    ... # see Simple App

def detect_cells(my_wsi: dict, my_rectangle: dict):
    """
    pretends to do something useful

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    wsi_tile = get_wsi_tile(my_wsi, my_rectangle)
    _ = wsi_tile  # wsi_tile not used in dummy code below

    cells = {
        "item_type": "point",
        "items": [],
        "reference_id": my_rectangle["id"],  # point annotations collection references my_rectangle
        "reference_type": "annotation",
        "type": "collection",  # NEW required in v3 apps
        "creator_type": "job",  # NEW required in v3 apps
        "creator_id": JOB_ID,  # NEW required in v3 apps
    }

    # your computational code below
    for _ in range(10):
        cell = {
            "name": "cell",
            "type": "point",
            "reference_id": my_wsi["id"],  # each point annotation references my_wsi
            "reference_type": "wsi",
            "coordinates": [250, 250],  # Always use WSI base level coordinates
            "npp_created": 499,  # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
            "npp_viewing": [
                499,
                3992,
            ],  # (optional) recommended pixel reslution range for viewer to display annotation, if npp_created is not sufficient
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        }
        cells["items"].append(cell)

    return cells


my_wsi = get_input("my_wsi")
my_rectangles = get_input("my_rectangles")

my_cells = {
    "item_type": "collection",
    "items": [],
    "type": "collection",  # NEW required in v3 apps
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

for my_rectangle in my_rectangles["items"]:
    cells = detect_cells(my_wsi, my_rectangle)
    my_cells["items"].append(cells)

post_output("my_cells", my_cells)

put_finalize()
```

For more information about the annotation metadata, please refer to the [Annotation Data Types](specs/ead.md#annotation-data-types) and [Annotation Resolution](specs/annotation_resolution#) sections.
