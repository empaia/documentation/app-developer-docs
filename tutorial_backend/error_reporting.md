# Error Reporting

This section demonstrates how to propagate an error to the user. The cause of an error can be diverse. The most common problem is that inputs do not meet algorithmic requirements, even though their statically verifiable properties do. For example, a region of interest needs to have a specific size to be processable by the algorithm. Always remember to fail fast, checking constraints before doing the computational expensive processing, to make the user experience as pleasant as possible.

In this section we will:

1. Create an EAD for a simple app specifying:
    * Single WSI as input.
    * Single rectangle annotation as input.
    * Single integer output.
2. Write the code that is needed to talk to the API to receive the specified rectangle.
3. Conditionally let the job fail with an error message to the user (depending on the rectangle's size).

## Inputs

* 1 WSI (key: `my_wsi`).
* 1 Rectangle annotation (key: `my_rectangle`) specifying the region to be processed.

## Outputs

* 1 Tumor cell count (key: `tumor_cell_count`).

## EAD

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Tutorial App 08 v3",
    "name_short": "TA08v3",
    "namespace": "org.empaia.vendor_name.tutorial_app_08.v3.0",
    "description": "Human readable description",
    "io": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangle": {
            "type": "rectangle",
            "reference": "io.my_wsi"
        },
        "tumor_cell_count": {
            "type": "integer"
        }
    },
    "modes": {
        "standalone": {
            "inputs": [
                "my_wsi",
                "my_rectangle"
            ],
            "outputs": [
                "tumor_cell_count"
            ]
        }
    }
}
```

## App API Usage

```python
import os
from io import BytesIO
from typing import Union

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    ... # see Simple App

def get_input(key: str, shallow: bool = False):
    ... # see Simple App

def post_output(key: str, data: dict):
    ... # see Simple App

def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    ... # see Simple App

def put_failure(error: Union[str, Exception]):
    """
    post error text to the API /failure endpoint as feedback to user

    Parameters:
        error: either an exception or simple string to inform the user of why the app failed
    """
    url = f"{APP_API}/v3/{JOB_ID}/failure"

    data = {"user_message": str(error)}

    r = requests.put(url, json=data, headers=HEADERS)
    r.raise_for_status()


def count_tumor_cells(wsi_tile: Image):
    """
    pretends to do something useful

    Parameters:
        wsi_tile: WSI image tile
    """
    return 42


class InputRegionTooSmall(Exception):
    pass


try:
    my_wsi = get_input("my_wsi")
    my_rectangle = get_input("my_rectangle")

    min_rectangle_side = 1024

    for side in ("width", "height"):
        if my_rectangle[side] < min_rectangle_side:
            raise InputRegionTooSmall(f"Input rectangle must have {side} greater than {min_rectangle_side}")

    wsi_tile = get_wsi_tile(my_wsi, my_rectangle)

    tumor_cell_count = {
        "name": "cell count tumor",  # choose name freely
        "type": "integer",
        "value": count_tumor_cells(wsi_tile),
        "creator_type": "job",  # NEW required in v3 apps
        "creator_id": JOB_ID,  # NEW required in v3 apps
    }

    post_output("tumor_cell_count", tumor_cell_count)

    put_finalize()

except Exception as e:
    put_failure(e)
```
