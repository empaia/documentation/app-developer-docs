# Output References Output

In order to decide if a cell is tumorous or not, your app calculates a confidence score. To not lose this information you decide to also save these scores, attached to the cell annotations.

In this section we will:

* Add a collection of collections of floats as outputs.
* Add a reference from one output to another output.

## Inputs

* 1 WSI (key: `my_wsi`).
* 1 Collection of many rectangle annotations (key: `my_rectangles`) specifying the regions to be processed.

## Outputs

* 1 Collection of collections of many tumor cell point annotations (key: `tumor_cells`). Each of the inner collections references one of the input rectangles from the `my_rectangles` collection.
* 1 Collection of collections of many confidence scores (key: `confidence_scores`). Each confidence score refers to one of the tumor cell point annotation results.

## EAD

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Tutorial App 04 v3",
    "name_short": "TA04v3",
    "namespace": "org.empaia.vendor_name.tutorial_app_04.v3.0",
    "description": "Human readable description",
    "io": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangles": {
            "type": "collection",
            "items": {
                "type": "rectangle",
                "reference": "io.my_wsi"
            }
        },
        "my_cells": {
            "type": "collection",
            "items": {
                "type": "collection",
                "reference": "io.my_rectangles.items",
                "items": {
                    "type": "point",
                    "reference": "io.my_wsi"
                }
            }
        },
        "my_confidences": {
            "type": "collection",
            "items": {
                "type": "collection",
                "items": {
                    "type": "float",
                    "reference": "io.my_cells.items.items"
                }
            }
        }
    },
    "modes": {
        "standalone": {
            "inputs": [
                "my_wsi",
                "my_rectangles"
            ],
            "outputs": [
                "my_cells",
                "my_confidences"
            ]
        }
    }
}
```

## App API Usage

```python
import os
from io import BytesIO

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    ... # see Simple App

def get_input(key: str):
    ... # see Simple App

def post_output(key: str, data: dict):
    ... # see Simple App

def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    ... # see Simple App

def detect_cells(my_wsi: dict, my_rectangle: dict):
    """
    pretends to do something useful

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    wsi_tile = get_wsi_tile(my_wsi, my_rectangle)
    _ = wsi_tile  # wsi_tile not used in dummy code below

    cells = {
        "item_type": "point",
        "items": [],
        "reference_id": my_rectangle["id"],  # point_annotations collection references my_rectangle
        "reference_type": "annotation",
        "type": "collection",  # NEW required in v3 apps
        "creator_type": "job",  # NEW required in v3 apps
        "creator_id": JOB_ID,  # NEW required in v3 apps
    }

    confidences = {
        "item_type": "float",
        "items": [],
        "type": "collection",  # NEW required in v3 apps
        "creator_type": "job",  # NEW required in v3 apps
        "creator_id": JOB_ID,  # NEW required in v3 apps
    }

    # your computational code below
    for _ in range(10):
        cell = {
            "name": "cell",
            "type": "point",
            "reference_id": my_wsi["id"],  # each point annotation references my_wsi
            "reference_type": "wsi",
            "coordinates": [250, 250],  # always use WSI base level coordinates
            "npp_created": 499,  # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
            "npp_viewing": [
                499,
                3992,
            ],  # (optional) recommended pixel reslution range for viewer to display annotation, if npp_created is not sufficient
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        }
        cells["items"].append(cell)

        confidence = {
            "name": "confidence score",
            "type": "float",
            "value": 0.9,
            "reference_id": None,  # yet unknown
            "reference_type": "annotation",
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        }
        confidences["items"].append(confidence)

    return cells, confidences


my_wsi = get_input("my_wsi")
my_rectangles = get_input("my_rectangles")

my_cells = {
    "item_type": "collection",
    "items": [],
    "type": "collection",  # NEW required in v3 apps
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

my_confidences = {
    "item_type": "collection",
    "items": [],
    "type": "collection",  # NEW required in v3 apps
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

for my_rectangle in my_rectangles["items"]:
    cells, confidences = detect_cells(my_wsi, my_rectangle)
    my_cells["items"].append(cells)
    my_confidences["items"].append(confidences)

my_cells = post_output("my_cells", my_cells)  # response includes IDs in addition to the original data

for cells, confidences in zip(my_cells["items"], my_confidences["items"]):
    for cell, confidence in zip(cells["items"], confidences["items"]):
        confidence["reference_id"] = cell["id"]  # id from POST response

post_output("my_confidences", my_confidences)

put_finalize()
```

* When you save any result, you receive the persisted result as a JSON response including its assigned `id`. This `id` can be used by further results as a `reference_id`.
* When saving a nested result (collection), you receive a nested response.
