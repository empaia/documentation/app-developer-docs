# Postprocessing App

The app in this example omits the `standalone` mode and declares a `preprocessing` and **containerized** `postprocessing` app mode. A containerized postprocessing app mode could be necessary, if more complex calculations or image analysis tasks are needed that may exceed the capacities of the App UI.

The application scenarios of the different app modes are briefly described and inputs and outputs of each mode are listed.

## Mode: Preprocessing

Processes a single WSI and generates polygon annotations for detected cells.

### Inputs

* 1 WSI (key: my_wsi).

### Outputs

* 1 Polygon annotation collection (key: my_cells), where each result annotation references the input WSI.

## Mode: Postprocessing

Generates classification results and a tumor ratio for cells within a specified RoI. The classification of the cells and the calcutation of the tumor ratio must be done in the containerized app because the mode is declared as containerized in the EAD:

```JSON
"postprocessing": {
    "containerized": true,
    "inputs": [
        "my_wsi",
        "my_rectangle",
        "my_cells"
    ],
    "outputs": [
        "my_cell_classes",
        "tumor_ratio"
    ]
}
```

### Inputs

* 1 WSI (key: `my_wsi`).
* 1 Rectangle annotation (key: `my_rectangle`) specifying the region to be processed (references the input WSI).
* 1 Polygon annotation collection (key: `my_cells`), where each annotation references the input WSI.

### Outputs

* 1 Class collection (key: `my_cell_classes`), where each class result references one of the polygon annotations.
  * Values for the classes must be declared in `"classes"` within the EAD
* 1 Float primitive (key: `tumor_ratio`), references the input rectangle.

## EAD

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Tutorial App 12 v3",
    "name_short": "TA12v3",
    "namespace": "org.empaia.vendor_name.tutorial_app_12.v3.0",
    "description": "Human readable description",
    "classes": {
        "tumor": {
            "name": "Tumor"
        },
        "non_tumor": {
            "name": "Non tumor"
        }
    },
    "io": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangle": {
            "type": "rectangle",
            "reference": "io.my_wsi"
        },
        "my_cells": {
            "type": "collection",
            "items": {
                "type": "polygon",
                "reference": "io.my_wsi"
            }
        },
        "my_cell_classes": {
            "type": "collection",
            "items": {
                "type": "class",
                "reference": "io.my_cells.items"
            }
        },
        "tumor_ratio": {
            "type": "float",
            "reference": "io.my_rectangle"
        }
    },
    "modes": {
        "preprocessing": {
            "inputs": [
                "my_wsi"
            ],
            "outputs": [
                "my_cells"
            ]
        },
        "postprocessing": {
            "containerized": true,
            "inputs": [
                "my_wsi",
                "my_rectangle",
                "my_cells"
            ],
            "outputs": [
                "my_cell_classes",
                "tumor_ratio"
            ]
        }
    }
}
```
