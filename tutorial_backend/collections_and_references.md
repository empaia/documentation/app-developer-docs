# Collections and References

Chances are high that a user wants to analyze multiple parts of a WSI. Therefore the EAD allows apps to receive a collection of multiple objects, in this case rectangle annotations.

In this section we will:

* Introduce collections as input and output.
* Introduce references to items of a collection.
* Introduce references to a collection.

## Inputs

* 1 WSI (key: `my_wsi`).
* 1 Collection of many rectangle annotations (key: `my_rectangles`) specifying the regions to be processed.

## Outputs

* 1 Collection of many tumor cell counts (key: `tumor_cell_counts`), where each result tumor cell count references a rectangle annotation.
* 1 Average tumor cell count (key: `avg_tumor_cell_count`), computed across all rectangle annotations and therefore referencing a collection.

## EAD

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Tutorial App 02 v3",
    "name_short": "TA02v3",
    "namespace": "org.empaia.vendor_name.tutorial_app_02.v3.0",
    "description": "Human readable description",
    "io": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangles": {
            "type": "collection",
            "items": {
                "type": "rectangle",
                "reference": "io.my_wsi"
            }
        },
        "tumor_cell_counts": {
            "type": "collection",
            "items": {
                "type": "integer",
                "reference": "io.my_rectangles.items"
            }
        },
        "avg_tumor_cell_count": {
            "type": "float",
            "reference": "io.my_rectangles"
        }
    },
    "modes": {
        "standalone": {
            "inputs": [
                "my_wsi",
                "my_rectangles"
            ],
            "outputs": [
                "tumor_cell_counts",
                "avg_tumor_cell_count"
            ]
        }
    }
}
```

* Parameters of `"type": "collection"` must include an `"items"` property to specify the type of items contained inside the collection.
* The reference parameter is mandatory for annotations and classes, but optional for other types. Its effect differs depending if it is used within an input or output parameter.
  * Inputs use references as constraint. Here, each rectangle annotation of `my_rectangles` has to be an annotation referencing the input WSI `my_wsi`.
  * Outputs use references for semantic purposes. Here, each integer value of the collection `tumor_cell_counts` will refer to one rectangle annotation of `my_rectangles`.
  * If the reference parameter is not defined in the EAD, the app will not be able to include a reference when sending results. Therefore it will not be possible to determine which value refers to which rectangle annotation.
  * As the `avg_tumor_cell_count` is a result of all inspected rectangle annotations, we can use the `reference` property to point to the whole collection `my_rectangles` instead of a single rectangle annotation.

If you are curious how references might be used, have a look at [Purpose of References](specs/references.md#).

## App API Usage

```python
import os
from io import BytesIO

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    ... # see Simple App

def get_input(key: str):
    ... # see Simple App

def post_output(key: str, data: dict):
    ... # see Simple App

def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    ... # see Simple App

def count_tumor_cells(wsi_tile: Image):
    ... # see Simple App

def compute_avg(tumor_cell_counts: dict):
    """
    pretends to do something useful

    Parameters:
        tumor_cell_counts: contains list of items with numeric value
    """
    values = [item["value"] for item in tumor_cell_counts["items"]]
    return sum(values) / len(values)


my_wsi = get_input("my_wsi")
my_rectangles = get_input("my_rectangles")

tumor_cell_counts = {
    "item_type": "integer",
    "items": [],
    "type": "collection",
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

for my_rectangle in my_rectangles["items"]:
    wsi_tile = get_wsi_tile(my_wsi, my_rectangle)

    tumor_cell_count = {
        "name": "cell count tumor",  # choose name freely
        "type": "integer",
        "value": count_tumor_cells(wsi_tile),
        "reference_id": my_rectangle["id"],  # set reference to rectangle
        "reference_type": "annotation",
        "creator_type": "job",  # NEW required in v3 apps
        "creator_id": JOB_ID,  # NEW required in v3 apps
    }
    tumor_cell_counts["items"].append(tumor_cell_count)

avg_tumor_cell_count = {
    "name": "avg tumor cell count",
    "type": "float",
    "value": compute_avg(tumor_cell_counts),
    "reference_id": my_rectangles["id"],  # set reference to rectangle collection
    "reference_type": "collection",
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

post_output("tumor_cell_counts", tumor_cell_counts)
post_output("avg_tumor_cell_count", avg_tumor_cell_count)

put_finalize()
```
