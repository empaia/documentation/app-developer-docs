# Configuration Settings

Although we strongly suggest to handle all computation inside your app, we understand that this is not always easy to realize and may require a lot of implementation effort depending on the existing infrastructure (e.g. having a well established API where image data can be sent for processing). In order to enable an initial integration into the EMPAIA platform, a first step could be to implement an EMPAIA-compliant app serving as a proxy layer between our platform and your existing API. As an API is usually protected by credentials, the app has to hold the authentication secrets in order to communicate with the external backend. Hardcoding these values within the app is problematic as there is no possibility to change them quickly without providing a new container image. Those values can be stored as secrets in the EMPAIA marketplace. Other use cases for a global app configuration could be encryption keys or app settings. In addition, a customer config can also be created for an app, which applies in each case to the organization of the respective user.

**Note:** If the app sends data to an external API, the property `data_transmission_to_external_service_provider` must be included and set to `true`.

The configuration of an app can be split into two sections:

* global
  * configuration settings independent of the user / organization (e.g. API credentials or app settings)
* customer
  * configuration settings dependent of the user / organization (e.g. organization credentials)
* the different configurations are independent from each other, e.g. an app can use either or both

In this section we will:

* Add global and customer configuration settings.

## Configuration

* Global configuration:
  * private_api_username
  * private_api_password
  * optional_parameter
* Customer configuration:
  * customer_username
  * customer_password

## Inputs

* 1 WSI (key: `my_wsi`).
* 1 Rectangle annotation (key: `my_rectangle`) specifying the region to be processed.

## Outputs

* 1 Tumor cell count (key: `tumor_cell_count`).

## EAD

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Tutorial App 07 v3",
    "name_short": "TA07v3",
    "namespace": "org.empaia.vendor_name.tutorial_app_07.v3.0",
    "description": "Human readable description",
    "configuration": {
        "global": {
            "private_api_username": {
                "type": "string",
                "optional": false
            },
            "private_api_password": {
                "type": "string",
                "optional": false
            },
            "optional_parameter": {
                "type": "integer",
                "optional": true
            }
        },
        "customer": {
            "customer_username": {
                "type": "string",
                "optional": false
            },
            "customer_password": {
                "type": "string",
                "optional": false
            }
        }
    },
    "permissions": {
        "data_transmission_to_external_service_provider": true
    },
    "io": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangle": {
            "type": "rectangle",
            "reference": "io.my_wsi"
        },
        "tumor_cell_count": {
            "type": "integer"
        }
    },
    "modes": {
        "standalone": {
            "inputs": [
                "my_wsi",
                "my_rectangle"
            ],
            "outputs": [
                "tumor_cell_count"
            ]
        }
    }
}
```

## App API Usage

```python
import os
from io import BytesIO
from typing import List

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    ... # see Simple App

def get_input(key: str):
    ... # see Simple App

def post_output(key: str, data: dict):
    ... # see Simple App

def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    ... # see Simple App

def get_configuration():
    """
    gets configuration parameter by key
    """
    url = f"{APP_API}/v3/{JOB_ID}/configuration"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    data = r.json()
    # NEW subgroups "global" and "customer" in v3 apps
    return data.get("global"), data.get("customer")


def count_tumor_cells_external_api(wsi_tile: Image, user: str, password: str, parameter: int):
    """
    pretends to do request external api

    Parameters:
        wsi_tile: WSI image tile
        user: user for external api
        password: password for external apiwtte
        parameter: some parameter
    """
    return 42


my_wsi = get_input("my_wsi")
my_rectangle = get_input("my_rectangle")
wsi_tile = get_wsi_tile(my_wsi, my_rectangle)

config_global, config_customer = get_configuration()  # NEW subgroups "global" and "customer" in v3 apps
user = config_global["private_api_username"]
password = config_global["private_api_password"]  
parameter = config_global.get("optional_parameter", 84)

tumor_cell_count = {
    "name": "cell count cumor",  # choose name freely
    "type": "integer",
    "value": count_tumor_cells_external_api(wsi_tile, user, password, parameter),
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

post_output("tumor_cell_count", tumor_cell_count)

put_finalize()
```
