# Simple App

Suppose you have developed an algorithm which consumes a region of a Whole Slide Image (WSI) and counts the number of tumorous cells. Two things are needed to create an EMPAIA App from your algorithm:

1. An EMPAIA App Description (EAD) written in JSON
2. Some glue code to receive/send input/output data from/to a RESTful HTTP interface (API)

In this section we will:

1. Create an EAD for a simple app specifying:
    * Single WSI as input.
    * Single rectangle annotation as input.
    * Single integer output.
2. Write the code that is needed to talk to the API to receive the specified inputs and to send the specified output.

## IO Section

Data structures, that serve as inputs and outputs of an app, are initially defined in the `io` section of the EAD. In this section, the data, that is consumed and produced by the app, is defined only once. The app can then use this data as inputs and/or outputs for its' app modes (see [App Modes](specs/app_modes.md#)). These data fields define a type such as `wsi` or `integer` and can reference other data. In our example we need the following inputs and outputs:

### Inputs

* 1 WSI (key: `my_wsi`).
* 1 Rectangle annotation (key: `my_rectangle`) specifying the region to be processed.

### Outputs

* 1 Tumor cell count (key: `tumor_cell_count`).

## App Mode

In this example, the algorithm is not designed to process an entire WSI, but only a region specified by a RoI (rectangle annotation). Therefore the only app mode declared in the EAD is `standalone`.

?> More information about available [App Modes](specs/app_modes.md#).

## EAD

```JSON
{
    "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
    "name": "Tutorial App 01 v3",
    "name_short": "TA01v3",
    "namespace": "org.empaia.vendor_name.tutorial_app_01.v3.0",
    "description": "Human readable description",
    "io": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangle": {
            "type": "rectangle",
            "reference": "io.my_wsi"
        },
        "tumor_cell_count": {
            "type": "integer"
        }
    },
    "modes": {
        "standalone": {
            "inputs": [
                "my_wsi",
                "my_rectangle"
            ],
            "outputs": [
                "tumor_cell_count"
            ]
        }
    }
}
```

## Appearance in the Workbench Client

The appearance of the app in the Workbench is defined by:

* Your organisation's name in the EMPAIA portal
* Your organisation's logo in the EMPAIA portal
* The `short_name` property of your app's EAD (see above)
* The `description` property of your app's EAD (see above)

For the above EAD, this would be:

<img src="images/app_card_client.png"></img>

## App API Usage

Inside your app input and output parameters can be accessed via the App API. The endpoints are specified by the EAD:

```python
import os
from io import BytesIO

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    """
    get a WSI tile on level 0

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    x, y = my_rectangle["upper_left"]
    width = my_rectangle["width"]
    height = my_rectangle["height"]

    wsi_id = my_wsi["id"]
    level = 0

    tile_url = f"{APP_API}/v3/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{width}/{height}"
    r = requests.get(tile_url, headers=HEADERS)
    r.raise_for_status()

    return Image.open(BytesIO(r.content))


def count_tumor_cells(wsi_tile: Image):
    """
    pretends to do something useful

    Parameters:
        wsi_tile: WSI image tile
    """
    return 42


my_wsi = get_input("my_wsi")
my_rectangle = get_input("my_rectangle")

wsi_tile = get_wsi_tile(my_wsi, my_rectangle)

tumor_cell_count = {
    "name": "cell count tumor",  # choose name freely
    "type": "integer",
    "value": count_tumor_cells(wsi_tile),
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

post_output("tumor_cell_count", tumor_cell_count)

put_finalize()
```
