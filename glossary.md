# Glossary

## Auth Service

A service that provides functionality for authentication and authorization between services and clients in the EMPAIA ecosystem.
The auth service is always provided centrally via the EMPAIA platform.

## Annotation

Annotations in digital histopathology on WSIs. Supported Annotatations are: `point`, `line`, `arrow`, `rectangle`, `polygon` and `circle`, see also [Specs - EMPAIA-App-Description - Annotation Data Types](specs/ead.md#annotation-data-types).

## App

An app is a self-contained component for providing functionality and/or data processing via the EMPAIA architecture. Apps are shipped as container images and processed in the EMPAIA platform backend.

## App UI

An App UI is a web frontend, specifically designed to inferface with a certain App.

## App Service

The App Service implements the App API. It provides an abstraction layer for the communication between a (containerized) app and the Medical Data API. All communication takes place via this interface. The App Service contacts the Medical Data API and transparently transfers the requested, app-specific identifiers to IDs of the Medical Data API.

**Note:** Future versions: The App Service validates if the request of the app (input and output) aligns with EAD. Requests not specified by the apps EAD will be rejected.

## Case

Pseudonymized medical data about a patients case. It includes WSIs and associated metadata.

## Container Registry

Docker registry storing published containerized apps.

## Containerized App

A Containerized App is an encapsulated algorithm / encapsulated AI model for execution in a Job Execution Service. Containerized Apps are usually complex, time-consuming and non-time-critical processing tasks that must be executed on a powerful infrastructure, e.g. a GPU cluster.

## EMPAIA App Description (EAD)

The EMPAIA App Description (EAD) describes the app, supported app modes and the inputs and outputs with their corresponding data types. Data identifiers defined in the EAD are used to read and write data via the App API.

## EMPAIA App Test Suite (EATS)

The EMPAIA App Test Suite (EATS) is a set of services and tools to to help developers creating apps and App UIs for the EMPAIA ecosystem.

## Examination

An Examination contains a single [App](glossary.md#app) and all [Jobs](glossary.md#job) of the app and is always assigned to a [Case](glossary.md#case).

## IMS

Image Management System to store and deliver WSIs.

## Job

A job is a concrete execution of an app with concrete input and output parameters.

## Job Execution Service (JES)

The  Job Execution Service runs Containerized Apps in a compute cluster.

## Marketplace Service (MPS)

Stores all app related data and metadata. The marketplace also offers a global configuration store. E.g. for storing credentials used by an app to communicate with external APIs.

## Medical Data Service (MDS)

The Medical Data Service implements the Medical Data API. The supported data types include WSIs, WSI metadata, Jobs and Job results (primitive data, annotations, collections, see [Specs - EMPAIA-App-Description - Data Types](specs/ead.md#data-types)).

## Nanometers Per Pixel (NPP)

In digital pathology, the unit nanomater per pixel (npp) is a frequently used term to express the dimensions of tissue and objects on whole slide images. Typical values for WSIs are 500nm for 20x or 250nm for 40x magnification.

## Pixelmap

Pixelmap is a data type to store and retrieve tiles of pixel-wise raw data. The pixelmap tiles can be rendered as overlays on top of WSIs in an App UI. Pixelmaps are fully defined in [EMP-0102](https://gitlab.com/empaia/emps/-/blob/main/emp-0102.md).

## PLIS

Pathology Laboratory Information System. Sometimes also Anatomic Pathology Laboratory Information System (AP-LIS).

## Region of Interest (RoI)

Interesting region of a WSI defined by pathologists. In the EMPAIA infrastructure RoIs are annotations of type `rectangle`, `polygon` or `circle` classified as `org.empaia.global.v1.classes.roi`.

## Scope

A Scope defines a contextual frame for an App UI to restrict the access to the Workbench API. Every Scope has its own ID and is uniquely tied to the combination of examination ID, app ID and user ID.

## Vendor App Communication Interface (VACI)

The Vendor App Communication Interface (VACI) provides a set of functions to communicate with the WBC 2.0.

## Whole Slide Image (WSI)

Digitized slide.

## Workbench Client (WBC)

The Workbench Client is the central user interface for diagnostics and research of the EMPAIA platform.

## Workbench Service (WBS)

The Workbench Service implements the Workbench API. It allows the Workbench Client to connect to the EMPAIA infrastructure.
