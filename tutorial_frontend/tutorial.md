# Tutorial: Frontend

This tutorial covers the development of app-specific UIs using the Workbench API. The API provides endpoints to send and receive data to parameterize apps and visualize all app related data. This includes the creation of jobs with inputs as specified in the EAD. The App UI described in the subsequent sections is customized for the backend app covered previously in [Standalone App with UI - Backend part](tutorial_backend/standalone_with_ui.md#).

The source code of the software components described in the tutorial is available on GitLab:

- [Sample App UI](https://gitlab.com/empaia/integration/frontend-workspace/-/tree/master/apps/sample-app-v3)

?> Code examples in this tutorial are written in Python for backend code and JavaScript / TypeScript using Angular v15 for frontend code. Note, that you are free to use any other programming languages or frameworks for your implementation.

For detailed information about the EAD and the backend examples, see [Tutorial: Backend](tutorial_backend/tutorial.md#). For further information about the Workbench API, see [Workbench API](specs/workbench_api.md#). Also take a look at the [App UI Requirements](specs/app_ui_requirements.md#) specification.
