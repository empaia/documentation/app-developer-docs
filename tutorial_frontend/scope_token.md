# Scope Token

The Workbench v3 Scopes API allows an App UI to access the functionalities provided by the EMPAIA Platform. The API is protected and only accessible via a valid Scope Token. The Scope Token itself is a JSON Web Token (JWT). The previous section [App UI initialization](tutorial_frontend/initialization.md#) describes how a token is retrieved during the App UI startup, while the following paragraph deals with the use and renewal of the Scope Token.

Every request to the Workbench v3 Scopes API (`/v3/scopes/`) requires an authorization header for verification. The Scope Token received by `addTokenListener()` is therefore used to authenticate against the Scopes API. Keep in mind that the Scope Token expires after a certain amount of time. We recommend to proactively renew the token before it expires (e.g. by decoding the JWT) but it is also possible to renew the token after receiving an HTTP 401 error code (not authenticated).

!> You should never assume or hardcode a certain expiration time, as this setting might be subject to change.

Pure JavaScript implementation can use XMLHttpRequest. With Angular it is advised to use HttpInterceptors to add the authorization header to every request (as demonstrated in the Sample App UI).


<!-- tabs:start -->
<!-- tab:JavaScript -->
```js
const xhr = new XMLHttpRequest();
xhr.setRequestHeader('Authorization', `Bearer ${token.value}`);
// the actual request
```
<!-- tab:TypeScript -->
```js
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const authReq = request.clone({
      setHeaders: {
        ['Authorization']: `Bearer ${INSERT_TOKEN_HERE}`,
      }
    });

    return next.handle(authReq);
  }
}
```
<!-- tabs:end -->


If the token has expired, the App UI has to request a new one via the `requestNewToken()` function of the Vendor App Communication Interface.

<!-- tabs:start -->
<!-- tab:JavaScript -->
```js
function mySampleFunction() {
  vendorAppCommunication.addTokenListener(function(token) {
    // all added event listeners will receive the new token,
    // every time after the requestNewToken() function was
    // called
  });
}

function requestToken() {
  vendorAppCommunication.requestNewToken();
}
```
<!-- tab:TypeScript -->
```ts
import {
  addTokenListener,
  requestNewToken,
} from '@empaia/vendor-app-communication-interface';

function mySampleFunction() {
  addTokenListener((token: Token) => {
    // all added event listeners will receive the new token,
    // every time after the requestNewToken() function was
    // called
  });
}

function requestToken() {
  requestNewToken();
}
```
<!-- tabs:end -->
