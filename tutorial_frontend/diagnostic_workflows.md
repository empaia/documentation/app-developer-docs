# Diagnostic Workflows

The EMPAIA specification allows Apps to include a specialized App UI, that offers a certain diagnostic workflow to the users. An App UI cannot be started on its own, because it is always tied to a specific App and it is always embedded into an EMPAIA compatible Workbench using iframes. A Workbench can either be a third-party clinical system (for example a digital pathology Image Management System) or the EMPAIA Workbench Client, an open-source reference implementation.

Depending on the [modes](specs/app_modes.md#) implemented by an App, the possible workflows of the App UI may differ:

1. In the `standalone` mode an App relies on its App UI to create processing jobs via the Workbench API. For example, a pathologist opens the App UI via the Workbench, inspects slides, draws one or many regions of interest (RoI) and the App UI creates a job to process the RoI in the app backend. In this workflow the user first interacts with the App before the processing is triggered.
2. In the `preprocessing` mode an App relies on the platform or a third-party digitization pipeline to start jobs, that process full WSIs. This means, that the processing is triggered automatically without user interaction. As soon as the user opens the App UI via the workbench, job results might already be available for viewing.
3. In the `postprocessing` mode an App relies on its App UI to trigger postprocessing jobs based on user interactions. This allows a user to refine the results provided in a preprocessing job, for example by selecting hotspots, drawing a RoI or setting some classification threshold using a slider. A postprocessing job may either run in the App backend or in the App UI, depending on EAD settings.
4. In the `report` mode an App UI can store the main diagnostic results that have been reviewed by a user. The reported data may get transferred into a laboratory information system (depending on the clinical integration of the platform).

## Combining modes

Most App developers decide to either implement the `standalone` mode or to implement `preprocessing` and `postprocessing` modes. It is also possible to implement all modes in a single app, but App developers should carefully evaluate the complexity of the resulting diagnostic workflow. The following diagram illustrates an example workflow with pre-/postprocessing steps and an optional fallback to the standalone mode if preprocessing results are not (yet) available.

<img src="images/postprocessing.svg"></img>


