# App UI Initialization

The App UI is provided by the app developer and is supposed to work with a specific EAD and app. The App UI itself must be implemented as a single-page application (SPA). The Workbench Client (WBC) dynamically embeds App UIs in sandboxed iframes. This part of the tutorial describes the initialization process of an App UI and the communication between App UI and WBC. The following sections then describe how the App UI interacts with the Workbench v3 Scopes API. This API is protected through a Scope ID and a Token, that grants the App UI access to post and retrieve data from the backend. We will take a closer look at that concept in the section [Workbench API](specs/workbench_api.md#).

The following figure shows the main software components involved in the process.

<img src="images/wbs_api.svg"></img>

The general communication flow between these components works as follows:

1. The user selects the app in the left menu panel of the WBC. The WBC then requests a temporary Frontend Token that gives access to the static resources of the App UI (e.g. index.html, *.js, *.css).
2. The WBC 3.0 builds a URL including the Frontend Token to access the Workbench v3 Frontends API and sets the URL as `src` in a new iframe.
3. The iframe loads the index.html (or .htm) of the App UI and other static resources linked in the index.html file. Please note, that the Frontend Token expires after one minute. Therefore all resources must be downloaded immediately and should not be requested on demand.
4. The App UI loads the [Vendor App Communication Interface](https://gitlab.com/empaia/integration/frontend-workspace/-/tree/master/libs/vendor-app-communication-interface) on startup and registers event listeners to receive initial information (Scope ID, Scope Token and Workbench API Base URL) from the WBC.
5. The WBC is automatically informed about the listener registration and sends the requested information back via the Vendor App Communication Interface.
6. As soon as the App UI receives the credentials it can start using the Workbench v3 Scopes API at `${WBS_BASE_URL}/v3/scopes/`.
7. A Scope Token has a limited lifetime, but the App UI can request a new Scope Token via the Vendor App Communication Interface at any point in time.

## Requirements

* Node.js (version >= 16) and NPM (version >= 8)
  * Node.js for every supported operating system can be found here: https://nodejs.org/en/download/package-manager/
* Create a new App UI single-page application with or without a JavaScript framework of your choice.
* Include the Vendor App Communication Interface Library in the App UI.

## About the Vendor App Communication Interface

The Vendor App Communication Interface (VACI) is a library that provides a set of functions to communicate with the WBC. It is written in TypeScript and compiles to JavaScript with built-in type declarations.

### Install the npm package

Using npm as dependency management tool, install the latest version with:

`npm install @empaia/vendor-app-communication-interface`

The package can be found at: https://www.npmjs.com/package/@empaia/vendor-app-communication-interface

### Include via script tag

To include the Vendor App Communication Interface as a script tag, download the library from [here](https://www.jsdelivr.com/package/npm/@empaia/vendor-app-communication-interface). Right click on `/npm/@empaia/vendor-app-communication-interface@version/index.umd.js` and 'Save link as...' to store the library on your machine.\
Place the `index.umd.js` into a directory in your project. For example in `lib/index.umd.js`. Include the VACI into your `index.html`

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>My Vendor App</title>
    <style>
      body { margin: 0; }
    </style>
    <script src="lib/index.umd.js"></script>
  </head>
  <body>
    <script>
      // Your JavaScript Code
    </script>
  </body>
</html>
```

## How to use the VACI library

The VACI library uses an observer pattern with event listeners which accepts callback functions to inform your application about data being received from the WBC. The library also allows to request data on demand. Please set up the listeners as soon as your App UI starts. The WBC will be informed about the first listener registered for each event type and will send the required information back immediately.

<!-- tabs:start -->
<!-- tab:JavaScript -->
```js
function mySampleFunction() {
  vendorAppCommunication.addScopeListener(function(scope) {
    // the scope object contains the attribute id which is the scopeId
    const scopeId = scope.id;
    // store the scope id in your app
  });

  vendorAppCommunication.addTokenListener(function(token) {
    // the token object contains the attribute value which is the access token
    const accessToken = token.value;
    // store the access token in your app
  });

  vendorAppCommunication.addWbsUrlListener(function(wbsUrl) {
    // the wbsUrl object contains the attribute url which is the base url to the Workbench Service
    const url = wbsUrl.url;
    // store the Workbench Service URL in your app for the base URL for further api calls
  });
}
```
<!-- tab:TypeScript -->
```ts
import {
  addScopeListener,
  addTokenListener,
  addWbsUrlListener,
  Scope,
  Token,
  WbsUrl
} from '@empaia/vendor-app-communication-interface';

function mySampleFunction() {
  addScopeListener((scope: Scope) => {
    // the scope object contains the attribute id which is the scopeId
    const scopeId = scope.id;
    // store the scope id in your app
  });

  addTokenListener((token: Token) => {
    // the token object contains the attribute value which is the access token
    const accessToken = token.value;
    // store the access token in your app
  });

  addWbsUrlListener((wbsUrl: WbsUrl) => {
    // the wbsUrl object contains the attribute url which is the base url to the Workbench Service
    const url = wbsUrl.url;
    // store the Workbench Service URL in your app for the base URL for further api calls
  });
}
```
<!-- tabs:end -->
